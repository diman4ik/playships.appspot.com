package com.appspot.battleship;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Map;

import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.KeyFactory;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.appengine.api.users.User;
import com.google.appengine.api.users.UserService;
import com.google.appengine.api.users.UserServiceFactory;


@SuppressWarnings("serial")
public class Playships_appspot_comServlet extends HttpServlet {

    @Override
    public void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws IOException {
        UserService userService = UserServiceFactory.getUserService();
        User user = userService.getCurrentUser();
        
        if( user != null ){        
	        String user_name = user.getNickname();
	        	
	        resp.setCharacterEncoding("UTF-8");
	        resp.setContentType("text/html");
	        PrintWriter out = resp.getWriter();
	        
	        String playerKey = req.getParameter("playerKey");
	        
	        Map resp_map = null;
	        
	        if( ( playerKey == null ) || ( playerKey.length() == 0 ) ) {
	        	
	        	if( !SeaFightServer.checkOvercrowded() ) {
		        	Entity ent = null;
			        if( ( ent = SeaFightServer.checkPlayer( user_name ) ) == null ) {
			        	ent = SeaFightServer.createPlayer( user_name );
			        }

			        if( !SeaFightServer.playerBanned( ent ) ) {
		        		SeaFightServer.setPlayerOnline( ent );
		        		playerKey = KeyFactory.keyToString( ent.getKey() ); 
		        		resp_map = SeaFightServer.addPlayer( playerKey, user_name );
			        }
			        else {
			        	resp_map = SeaFightProtocol.assembleBannedPacket();
			        }
	        	}
	        	else {
	        		resp_map = SeaFightProtocol.assembleOvercrowdedPack();
	        	}
	        }
	        else {
	        	resp_map = SeaFightServer.processRequest( user_name, req.getParameterMap() );
	        }
	        	        
	        if( resp != null ) {
	        	out.print( SeaFightServer.responseToJson( resp_map ) );
	        }        
        }
    }
}
