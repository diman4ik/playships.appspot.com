package com.appspot.battleship;

import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import com.google.appengine.api.channel.ChannelFailureException;
import com.google.appengine.api.channel.ChannelMessage;
import com.google.appengine.api.channel.ChannelService;
import com.google.appengine.api.channel.ChannelServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.memcache.MemcacheService;
import com.google.appengine.api.users.UserServiceFactory;
import com.google.developers.ShardedCounter;


public class SeaFightServer {
	
	private static final Logger log = Logger.getLogger(SeaFightServer.class.getName());
	

	public static Map processRequest( String user_name, Map pack ) {
		String stype = ( (String[] ) pack.get( "type" ) )[0];

		int itype = Integer.parseInt(stype);

		if( itype == SeaFightProtocol.clientPackets.invite.value() ) {
			SeaFightProtocol.processInvitePack( user_name, pack );
		}
		else if( itype ==  SeaFightProtocol.clientPackets.invite_answer.value() ) {
			SeaFightProtocol.processInviteAnswerPack( user_name, pack );
		}
		else if( itype == SeaFightProtocol.clientPackets.am_ready.value() ) {
			SeaFightProtocol.processAmReadyPack( user_name, pack );
		}
		else if( itype == SeaFightProtocol.clientPackets.myMove.value() ) {
			SeaFightProtocol.processMyMovePack( user_name, pack );
		}
		else if( itype == SeaFightProtocol.clientPackets.leave.value() ) {
			removePlayer(user_name);
		}
		else if( itype == SeaFightProtocol.clientPackets.player_list_update.value() ) {
			Map ret = SeaFightProtocol.assemblePlayerListPacket();
			String message = responseToJson( ret );
			sendChannelMessageToUser( user_name, message );
		}
		else if( itype == SeaFightProtocol.clientPackets.stop_game.value() ) {
			SeaFightProtocol.processGameStopPacket( user_name, pack );
		}
		else if( itype == SeaFightProtocol.clientPackets.request_token.value() ) {
			String userId = UserServiceFactory.getUserService().getCurrentUser().getUserId();
			ChannelService channelService = ChannelServiceFactory.getChannelService();
			String token = channelService.createChannel(userId);
			
			Map resp = SeaFightProtocol.assembleNewTokenPacket( token );
			return resp;
		}
		else {
			log.warning( "Unknown packet from " + user_name );
		}
		
		return null;
	}

	public static String responseToJson( Map pack ) {
		String response = "";
		if( pack != null ) {
			response += "{";
			for( Object entry : pack.entrySet() ){
				response += ((Map.Entry)entry ).getKey() + ":" + ((Map.Entry)entry ).getValue() + ",";
			}

			// Remove trail comma
			if( response.length() > 1 )
				response = response.substring( 0, response.length() - 1 );
			response += "}";
		}
		
		return response;
	}

	public static Entity createPlayer( String name ) {
		SeaFightPlayer novice = new SeaFightPlayer(name);
		novice.setUserId( 
				UserServiceFactory.getUserService().getCurrentUser().getUserId() );
		Entity novice_entity = novice.getEntity();
		Util.persistEntity( novice_entity );
		return novice_entity;
	}
	
	public static void setPlayerOnline( Entity entity ) {
		SeaFightPlayer novice = SeaFightPlayer.fromEntity( entity );
		setPlayerOnline( novice );
	}
	
	public static void setPlayerOnline( SeaFightPlayer player ) {
		player.setOnline();
		player.setInGame( false );
		savePlayer(player);
		incCurUserCount();
		
		setPlayesListUpdates( player.getName(), true );
	}
	
	public static void setPlayerOffline( SeaFightPlayer player ) {
		player.setOffline();
		player.setInGame( false );
		savePlayer(player);
		decCurUserCount();
		
		setPlayesListUpdates( player.getName(), false );
	}

	public static Map addPlayer( String playerKey, String name )
	{
		// Channel creation
		String userId = UserServiceFactory.getUserService().getCurrentUser().getUserId();
		
		asocClientIdName( name, userId );
		
		ChannelService channelService = ChannelServiceFactory.getChannelService();
		String token = channelService.createChannel(userId);
		
		Map resp = SeaFightProtocol.assembleTokenPacket( name, playerKey, token );
		
		return resp;
	}

	private static void removePlayer( String player_name ) {
		log.info( player_name  + " leaves");
		
		SeaFightPlayer leaving = getPlayer( player_name );
		setPlayerOffline(leaving);		
	}

	public static SeaFightPlayer getPlayer(String name) {
		Key key = KeyFactory.createKey("Player", name);
		Entity ent = Util.findEntity( key );
		
		return SeaFightPlayer.fromEntity( ent );
	}

	public static Entity checkPlayer(String user_name) {
		/**
		 * Check if player exists in database
		 * 
		 */
		Key key = KeyFactory.createKey("Player", user_name);
		Entity ent = Util.findEntity( key );
		
		return ent;
	}
	
	public static Iterable<Entity> getPlayers() {
		Iterable<Entity> entities = Util.listEntities("Player", null, null);
		return entities;
	}

	public static void setPlayesListUpdates(String name, boolean entering) {		

		ChannelService channelService = ChannelServiceFactory.getChannelService();
		List<String> keys = getUseridListFromCacahe();
		
		if( keys == null )
			return;
		
		Map pack = null;
		
		if( entering )
			pack = SeaFightProtocol.assemblePlayerEnteredPacket(name);
		else
			pack = SeaFightProtocol.assemblePlayerLeavedPacket(name);
		
		for( String k : keys ) {
			channelService.sendMessage(new ChannelMessage(k, responseToJson(pack)));
		}
	}
	
	public static String constructPlayerListMessage() {
		Iterable<Entity> entities = Util.listEntities("Player", null, null);
		
		if( entities.iterator().hasNext() ) {
	
			String namesScores = "";
	
			for( Entity ent : entities ) {			
				if( (Boolean )ent.getProperty( "online" ) ) {				
					String name = (String )ent.getProperty( "name" );
					
					boolean playing = (Boolean )ent.getProperty( "inGame" );
					
					if(!playing)
			        	namesScores += name + " r";
			        else
			        	namesScores += name + " n";
					
					namesScores += ",";
				}
			}
			
			return namesScores;
		}
		
		return "";
	}

	public static SeaFightGame createGame( String player1, String player2 ) {
		SeaFightGame gm = new SeaFightGame( player2, player1 );

		saveGame( gm );

		return gm;
	}
	
	public static void changePlayerStatus( SeaFightPlayer player, boolean val ) {
		player.setInGame( !val );
		SeaFightServer.savePlayer( player );
		
		SeaFightServer.sendChannelMessageToAll( 
				SeaFightServer.responseToJson( 
						SeaFightProtocol.assemblePlayerStatusChanged( player.getName(), val ) )  );	
	}	
	public static void changePlayerStatus( String player_name, boolean val ) {
		changePlayerStatus( getPlayer( player_name ), val );
	}
	
	public static SeaFightGame getGame( String gameKey ) {
		Entity ent = Util.getFromCache( KeyFactory.stringToKey( gameKey ) );
		
		if( ent == null )
			return null;
		
		SeaFightGame gm = SeaFightGame.fromEntity( ent );
		return gm;
	}
	
	public static void saveGame( SeaFightGame gm ) {
		Entity ent = gm.getEntity();
		Util.addToCache( ent.getKey(), ent );
	}
	
	public static void removeGame( SeaFightGame gm ) {
		Entity ent = gm.getEntity();
		Util.deleteFromCache( ent.getKey()  );
	}
	
	public static void sendChannelMessageToUser( SeaFightPlayer player, String message ) {
		String userid = player.getUserId();
		ChannelService channelService = ChannelServiceFactory.getChannelService();
		
		try {
			channelService.sendMessage( new ChannelMessage( userid, message ) );
		}
		catch( ChannelFailureException ex ) {
			log.warning( 
				"Error sending message to user " + player.getName() + " with userid " + userid + " " + ex.getMessage() );
		}
	}
	
	public static void sendChannelMessageToUser( String name, String message ) {
		sendChannelMessageToUser( getPlayer(name), message );
	}
	
	private static String userid_list_key = "token_list"; 
	
	public static void addToUseridListCache( String name ) {
		Util.addToCacheList( userid_list_key, name );
	}
	
	public static List<String> getUseridListFromCacahe() {
		return Util.getCacheList( userid_list_key );
	}
	
	public static void deleteFromUseridListCache( String name ) {
		Util.deleteFromCacheList( userid_list_key, name );
	}
	
	public static void sendChannelMessageToAll( String message ) {
		ChannelService channelService = ChannelServiceFactory.getChannelService();
		List<String> keys = getUseridListFromCacahe();
		
		for( String k : keys ) {
			channelService.sendMessage(new ChannelMessage(k, message));
		}
	}

	public static void savePlayer( SeaFightPlayer player ) {
		Util.persistEntity( player.getEntity() );
	}
	
	public static void setPlayerField( String gameKey, String playerName, String field ) {
		// 1. get from cache
		// 2. set ready for player
		// 3. check if changed
		// 4. merge
		// 5. save
		
		Key key = KeyFactory.stringToKey( gameKey );
		
		MemcacheService.IdentifiableValue value = Util.getShared( key );
		SeaFightGame game = SeaFightGame.fromEntity( (Entity )value.getValue() );
		game.setPlayerReady(playerName);
		
		if( !Util.setShared( key, value,  game.getEntity() ) ) {
			SeaFightGame game2 = getGame(gameKey);
			game.merge(game2);
		}
		
		saveGame(game);		

		SeaFightPlayer player = SeaFightServer.getPlayer(playerName);
		player.setField(field);
		savePlayer(player);
	}

	public static void bahnPlayer(String name) {
		SeaFightPlayer player = getPlayer(name);
		player.bahn();

		savePlayer(player);
	}

	public static void unbahnPlayer(String name) {
		SeaFightPlayer player = getPlayer(name);
		player.unbahn();

		savePlayer(player);
	}
	
	public static void setMaximumNumberOfUsers( int count ) {
		Entity ret = new Entity( "max_users", "server_maximum_users" );
		ret.setProperty( "count", count );
		
		Util.persistEntity(ret);
	}

	public static boolean checkOvercrowded() {
		
		int max_count = getMaxUsersCount();
		
		
		int current_count = getCurUserCount();
		log.info( "Current number of players " + current_count );
		
		if( current_count < max_count )
			return false;
		
		return true;
	}
	
	public static int getMaxUsersCount() {
		int max_count = 0;
		
		Key key = KeyFactory.createKey( "max_users", "server_maximum_users" );
		Entity ent = Util.findEntity( key );
		
		if( ent == null )
			return 20;
	
		try {
			max_count = (Integer)ent.getProperty("count");
		}
		catch( ClassCastException ex ) {
			max_count = ((Long)ent.getProperty("count")).intValue();
		}
		
		return max_count;
	}
	
	public static String counterKey = "current_user_count";
	
	public static int getCurUserCount() {
		Entity ent = Util.getFromCache( counterKey );
		int ret = 0;
				
		if( ent != null ) {
			ret = (int) ent.getProperty( "counter" );
		}
		else {
			ent = new Entity("counter", counterKey);
			ent.setProperty( "counter", 0 );
			Util.addToCache( counterKey, ent );
		}
		
		return ret;
	}
	
	public static void incCurUserCount() {
		MemcacheService.IdentifiableValue value = Util.getShared( counterKey );
		Entity ent = (Entity )value.getValue();
		int count = (int) ent.getProperty( "counter" );
		
		Entity ret = new Entity("counter", counterKey);
		ret.setProperty( "counter", count + 1 );
		
		if( !Util.setShared( counterKey, value,  ret ) ) {
			Entity ent2 = Util.getFromCache( counterKey );
			int count2 = (int) ent2.getProperty( "counter" );
			
			ret.setProperty( "counter", count2 + 1 );
		}
		
		Util.addToCache( counterKey, ret );
	}
	
	public static void decCurUserCount() {
		MemcacheService.IdentifiableValue value = Util.getShared( counterKey );
		Entity ent = (Entity )value.getValue();
		int count = (int) ent.getProperty( "counter" );
		
		Entity ret = new Entity("counter", counterKey);
		ret.setProperty( "counter", ( ( count - 1 ) >= 0 )? count - 1 : 0 );
		
		if( !Util.setShared( counterKey, value,  ret ) ) {
			Entity ent2 = Util.getFromCache( counterKey );
			int count2 = (int) ent2.getProperty( "counter" );
			
			ret.setProperty( "counter", ( ( count2 - 1 ) >= 0 )? count2 - 1 : 0 );
		}
		
		Util.addToCache( counterKey, ret );
	}

	public static boolean playerBanned(Entity ent) {
		SeaFightPlayer player = SeaFightPlayer.fromEntity(ent);
		return player.banned();
	}

	public static void channelDisconnected(String clientId) {
		deleteFromUseridListCache( clientId );
		
		String name = getNameByClientId(clientId);
		
		SeaFightPlayer player = getPlayer(name);
		
		if( player.online() )
			setPlayerOffline(player);
		
		desocClientIdName(name, clientId);
	}

	public static void channelConnected(String clientId) {
		addToUseridListCache(clientId);
		
		String name = getNameByClientId(clientId);
		
		SeaFightPlayer player = getPlayer(name);
		
		if( !player.online() )
			setPlayerOnline(player);
	}
	
	private static String user_ids_key = "user_ids_list"; 
	
	public static String getNameByClientId( String clientId ) {
		List<String> name_ids = Util.getCacheList( user_ids_key );
		
		for( String next : name_ids ) {
			String[] parts = next.split( ":" );
			
			String id = parts[1];
			
			if( id.compareTo( clientId ) == 0 ) {
				return parts[0];
			}
		}
		
		return "";
	}
	
	public static void asocClientIdName( String name, String clientId ) {
		Util.addToCacheList( user_ids_key, name + ":" + clientId );
	}
	
	public static void desocClientIdName( String name, String clientId ) {
		Util.deleteFromCacheList( user_ids_key, name + ":" + clientId );
	}
}
