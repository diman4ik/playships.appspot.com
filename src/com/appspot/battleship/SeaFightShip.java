package com.appspot.battleship;

import com.google.appengine.api.datastore.EmbeddedEntity;


public class SeaFightShip {
	enum direction
	{
		none(0),
		vertical(1),
		horizontal(2);

		private direction(int val)
		{
			m_value = val;
		}

		private int value()
		{
			return m_value;
		}

		private int m_value = 0;
	}

	enum shipType
	{
		none(0),
		one_part(1),
		two_part(2),
		three_part(3),
		four_part(4);

		private shipType( int type )
		{
			m_type = type;
		}

		private int value()
		{
			return m_type;
		}

		private int m_type = 0;
	}

	public direction m_direction = direction.none;
	public shipType m_type = shipType.none;

	public int m_length = 0;

	public int m_botRightX = 0;
	public int m_botRightY = 0;
	
	public int m_cell = 0;
	public int m_hits = 0;
	
	public String m_key = "";
	
	
	public boolean canAdd( int index ) {
		if( ( index%SeaFightGame.FIELD_SIZE == m_botRightX + 1 ) &&
			( index/SeaFightGame.FIELD_SIZE == m_botRightY ) )
			return true;

		if( ( index/SeaFightGame.FIELD_SIZE == m_botRightY + 1 ) &&
			( index%SeaFightGame.FIELD_SIZE == m_botRightX ) )
			return true;

		return false;
	}
	
	public EmbeddedEntity getEntity() {
		EmbeddedEntity ret = new EmbeddedEntity();
		
		ret.setProperty( "key", m_key );
		ret.setProperty( "length", m_length );
		ret.setProperty( "cell", m_cell );
		ret.setProperty( "direction", m_direction.value() );
		ret.setProperty( "hits", m_hits );
		
		return  ret;
	}
	
	public static SeaFightShip fromEntity( EmbeddedEntity ent ) {
		SeaFightShip ret = new SeaFightShip();
		
		ret.m_key = (String)ent.getProperty("key");

		try {
			ret.m_length = (Integer)ent.getProperty( "length" );
		}
		catch( ClassCastException ex ) {
			ret.m_length = ( (Long)ent.getProperty( "length" ) ).intValue();
		}
		
		try {
			ret.m_cell = (Integer)ent.getProperty( "cell" );
		}
		catch( ClassCastException ex ) {
			ret.m_cell = ((Long)ent.getProperty( "cell" )).intValue();
		}
		
		int dir = direction.horizontal.value();
			
		try {
			dir = (Integer)ent.getProperty( "direction" );
		}
		catch( ClassCastException ex ) {
			dir = ((Long)ent.getProperty( "direction" )).intValue();
		}
		
		if( dir == direction.horizontal.value() )
			ret.m_direction = direction.horizontal;
		
		if( dir == direction.vertical.value() )
			ret.m_direction = direction.vertical;
		
		try {
			ret.m_hits = (Integer)ent.getProperty( "hits" );
		}
		catch( ClassCastException ex ) {
			ret.m_hits = ( (Long)ent.getProperty( "hits" ) ).intValue();
		}
		
		return ret;
	}

	public boolean hitted(int index) {
		
		int next = m_cell;
		
		for( int i = 0; i < m_length; i++ ) {
			if( next == index ) {
				m_hits++;
				return true;
			}
			
			if( m_direction == direction.horizontal ) {
				next++;
			}
			else {
				next += SeaFightGame.FIELD_SIZE;
			}
		}
		
		
		return false;
	}
	
	public boolean killed() {
		return m_hits == m_length;
	}
}
