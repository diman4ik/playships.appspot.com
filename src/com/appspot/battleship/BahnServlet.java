package com.appspot.battleship;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.users.User;
import com.google.appengine.api.users.UserService;
import com.google.appengine.api.users.UserServiceFactory;

public class BahnServlet extends HttpServlet {
	
	private static final Logger log = Logger.getLogger(SeaFightServer.class.getName());
	
	private String _key = "bahn_list_cache";
	private int page_size = 10;
	
    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws IOException {
    	UserService userService = UserServiceFactory.getUserService();
        User user = userService.getCurrentUser();

        if (user != null) {
            resp.setContentType("text/html");
            if(userService.isUserAdmin()) {
            	saveAllPlayersToCache();
            	
            	// Show all users with a bahn button for each
            	resp.setCharacterEncoding("UTF-8");
            	resp.setContentType("text/html");
            	PrintWriter out = resp.getWriter();
            	
            	List<String> listp = Util.getCacheList(_key);
            	
            	int listpSize = listp.size();
            	
            	out.println("<html>");
                out.println("<head>");
                out.println("<meta charset='utf-8' />");
                out.println("<title>Администрирование/Бан</title>");
                out.println("<link rel='stylesheet' href='../css/main.css' type='text/css' />");
                out.println("<script type='text/javascript' src='../js/utils.playships.appspot.js'></script>");
                out.println("<script src='//ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js'></script>");
                out.println("</head>");
                out.println("<script>" +
                			"$(function(){ Utils(); Utils.page_size = " + page_size + "; Utils.total_size = " + listpSize );
                out.println("});</script>");
                out.println("<body>");
                out.println("<div class='bahn_div'>");
                out.println("<h1>It's bahn servlet.</h1>");
                out.println("<span>Максимальное число пользователей:</span><input id='user_count' type='text' value=" + SeaFightServer.getMaxUsersCount() + "></input><input type='button' value='Установить' onclick='Utils.setUserNum();'></input><p></p><br>");
                out.println("<table id='bahn_list' class='bahn_table'>");
                out.println("<thead>");
                out.println("<th>Имя пользователя</th>");
                out.println("<th>Число начатых игр</th>");
                out.println("<th>Число завершенных игр</th>");
                out.println("<th>Зобанить/Разбанить</th>");
                out.println("</thead>");
                out.println("<tbody>");
                
            	for( int i = 0; ( i < page_size ) && ( i < listpSize ); i++ ) {
            		String[] next = listp.get(i).split(",");
            		String name = next[0].substring(7);
            		name = name.substring( 0, name.length() - 1 );
            		String nstarted = next[1].substring(9);
            		String nfinished = next[2].substring(10);
            		boolean banned = Boolean.parseBoolean( next[3].substring(7) );
            		out.println("<tr><td>" + name + "</td><td>" + nstarted + "</td><td>" + nfinished + "</td><td>");
            		if( !banned )
            			out.println("<input type='button' value='Зобанить' onclick='Utils.bahn(" + i + ");'/></td></tr>");
            		else
            			out.println("<input type='button' value='Розбанить' onclick='Utils.bahn(" + i + ");'/></td></tr>");
            	}
            	out.println("</tbody>");
                out.println("</table>");
                out.println( "Total players: <span id='total_count'>" + listp.size() + "</span>" );
                
                if( page_size < listpSize ) {
                	out.println( "<br><hr><input type='button' class='prev_players' onclick='Utils.prev()'></input>" );
                	out.println( "<input type='button' class='next_players' onclick='Utils.next()'></input>" );
                }
                
                out.println("</div>");
                out.println("</body>");
                out.println("</html>");
            	
            } else {
                resp.getWriter().println("Hi, you are not allowed.");
            }
        } else {
            resp.sendRedirect(userService.createLoginURL(req.getRequestURI()));
        }
    }
    
    @Override
    public void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws IOException {
    	UserService userService = UserServiceFactory.getUserService();
        User user = userService.getCurrentUser();

        if (user != null) {
        	String type = req.getParameter("type");
        	
        	if( type.compareTo("bahn") == 0 ) {
        		String name = req.getParameter("name");
        		log.info( "Bahn user " + name  );
        		SeaFightServer.bahnPlayer(name);
        	}
        	else if( type.compareTo("max_users") == 0 ) {
        		String count = req.getParameter("count");
        		log.info( "Maximum users " + count );     
        		
        		SeaFightServer.setMaximumNumberOfUsers( Integer.parseInt(count) );
        	}        	
        	else if( type.compareTo("unbahn") == 0 ) {
        		String name = req.getParameter("name");
        		log.info( "Unbahn user " + name  );
        		SeaFightServer.unbahnPlayer(name);
        	}
        	else if( type.compareTo("list") == 0 ) {
        		
        		String player_list = "[";
        		
        		List<String> listp = Util.getCacheList(_key);
        		
        		int from_index = Integer.parseInt( req.getParameter("from") );
        		int count = Integer.parseInt( req.getParameter("count") );
        		
        		for( int i = 0; ( i < count ) && ( from_index + i < listp.size() ) ; i++ ) {
        			player_list += listp.get(from_index + i) + ",";
        		}
        		
        		if( player_list.length() > 1 )
    				player_list = player_list.substring( 0, player_list.length() - 1 );
        		
        		player_list += "]";
        		
        		resp.setCharacterEncoding("UTF-8");
    	        resp.setContentType("text/html");
    	        PrintWriter out = resp.getWriter();
    	        
    	        out.print(player_list);
    	        
    	        log.info("request completed");
        	}
        } else {
            resp.sendRedirect(userService.createLoginURL(req.getRequestURI()));
        }
    }
    
    public void saveAllPlayersToCache() {
    	Util.clearCacheList( _key );
    	
    	for( Entity ent : SeaFightServer.getPlayers() ) {
			SeaFightPlayer player = SeaFightPlayer.fromEntity(ent);
			String str = player.toString();
			
			Util.addToCacheList( _key, str );
		}
    }
}
