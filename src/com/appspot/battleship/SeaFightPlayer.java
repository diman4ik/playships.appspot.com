package com.appspot.battleship;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.appspot.battleship.SeaFightGame.moveRes;
import com.google.appengine.api.datastore.EmbeddedEntity;
import com.google.appengine.api.datastore.Entity;


public class SeaFightPlayer {
	
	private boolean m_online = false;
	private String m_name = "";
	private boolean m_inGame = false;
	private String m_userId = "";
	private List<SeaFightShip> m_playerShips = new ArrayList<SeaFightShip>();
	private int m_totalKilled = 0;
	private String m_field = "";
	private boolean m_banned = false;
	private int m_nstartedGames = 0;
	private int m_nfinishedGames = 0;
	
	
	public SeaFightPlayer( String name ) {
		m_name = name;
		m_inGame = false;
	}

	public void setOnline() {
		m_online = true;
	}
	
	public void setOffline() {
		m_online = false;
	}
	
	public boolean online() {
		return m_online;
	}
	
	public void setUserId( String userId ) {
		m_userId = userId;
	}
	
	public String getUserId() {
		return m_userId;
	}
	
	public String getName() {
		return m_name;
	}
	
	public void bahn() {
		m_banned = true;
	}
	
	public boolean banned() {
		return m_banned;
	}
	
	public String getField() {
		return m_field;
	}
	
	public void setInGame( boolean val ) {
		m_inGame = val;		
	}
	
	public void gameStarted() {
		m_inGame = true;
		m_nstartedGames++;
	}
	
	public void gameFinished() {
		m_inGame = false;
		m_nfinishedGames++;		
	}
	
	public boolean inGame() {
		return m_inGame;
	}
	
	public void setField( String field ) {
		m_field = field;
		setShips( SeaFightGame.findFieldShips(field) );
	}
	
	public void setShips( List<SeaFightShip> ships ) {
		m_playerShips = ships;
		
		int current3Field = 0;
		int current2Field = 0;
		int current1Field = 0;
		
		for( SeaFightShip ship : ships )
		{
			int length = ship.m_length;

			if( length == 1  ) {
				current1Field++;
				ship.m_key = "boat_" + current1Field; 
			}
			else if( length == 2 ) {
				current2Field++;
				ship.m_key = "destroyer_" + current2Field;
			}
			else if( length == 3 ) {
				current3Field++;
				ship.m_key = "cruiser_" + current3Field;
			}
			else if( length == 4 ) {
				ship.m_key = "carrier";
			}
		}
	}
	
	public SeaFightGame.moveRes getMoveRes( int index ) {
		if( ( m_field.charAt( index ) == SeaFightGame.fieldType.sea.value() ) ||
			( m_field.charAt( index ) == SeaFightGame.fieldType.ship.value() ) ) {
			StringBuilder newField = new StringBuilder(m_field);
			
			if( m_field.charAt( index ) == SeaFightGame.fieldType.sea.value() )
				newField.setCharAt( index, SeaFightGame.fieldType.miss.value() );
			if( m_field.charAt( index ) == SeaFightGame.fieldType.ship.value() )
				newField.setCharAt( index, SeaFightGame.fieldType.hit.value() );
			
			m_field = newField.toString();
		}
		else {
			return moveRes.illegal;
		}
		
		SeaFightGame.moveRes.gameover = false;
		
		SeaFightGame.moveRes ret = moveRes.miss;
		
		for( SeaFightShip ship : m_playerShips ) {
			if( ship.hitted( index ) ) {
				if( ship.killed() ) {
					m_totalKilled++;
					
					if( ship.m_length == 1 ) {
						ret = moveRes.boat_killed;
					}
					else if( ship.m_length == 2 ) {
						ret = moveRes.destroyer_killed;
					}
					else if( ship.m_length == 3 ) {
						ret = moveRes.cruiser_killed;
					}
					else {
						ret = moveRes.carrier_killed;
					}
					
					if( m_totalKilled == 10 ) {
						SeaFightGame.moveRes.gameover = true;
					}
					
					break;
				}
				
				ret = moveRes.hit;
				break;
			}
		}
		
		return ret;
	}

	public Entity getEntity() {
		Entity ret = new Entity("Player", m_name);
		
		ret.setProperty( "name", m_name );
		ret.setProperty( "online", m_online );
		ret.setProperty( "inGame", m_inGame );
		ret.setProperty( "userId", m_userId );
		ret.setProperty( "field", m_field );
		ret.setProperty( "banned", m_banned );
		ret.setProperty( "nstarted", m_nstartedGames );
		ret.setProperty( "nfinished", m_nfinishedGames );
		
		if( m_inGame ) {		
			ret.setProperty( "totalKilled", m_totalKilled );
			
			for( SeaFightShip ship : m_playerShips ) {
				ret.setProperty( ship.m_key, ship.getEntity() );
			}
		}
		
		return ret;
	}
	
	static SeaFightShip shipFromEntity( String key, Entity ent ) {
		EmbeddedEntity shent = ( EmbeddedEntity )ent.getProperty(key);
		if( shent != null )	{
			return SeaFightShip.fromEntity( shent );
		}
		
		return null;
	}
	
	public static SeaFightPlayer fromEntity( Entity ent ) {
		SeaFightPlayer ret = new SeaFightPlayer( (String )ent.getProperty("name") );
		
		ret.m_name 	 = (String )ent.getProperty( "name" );
		ret.m_online = (Boolean )ent.getProperty( "online" );
		ret.m_inGame = (Boolean )ent.getProperty( "inGame" );
		ret.m_userId = (String )ent.getProperty( "userId" );
		ret.m_field = (String)ent.getProperty("field");
		ret.m_banned = (Boolean)ent.getProperty("banned");
		
		try {
			ret.m_nstartedGames = (Integer)ent.getProperty("nstarted");
		}
		catch( ClassCastException ex ) {
			ret.m_nstartedGames = ( (Long)ent.getProperty("nstarted") ).intValue();
		}
		
		try {
			ret.m_nfinishedGames = (Integer)ent.getProperty("nfinished");
		}
		catch( ClassCastException ex ) {
			ret.m_nfinishedGames = ( (Long)ent.getProperty("nfinished") ).intValue();
		}
	
		if( ret.m_inGame ) {
			try {
				ret.m_totalKilled = (Integer)ent.getProperty("totalKilled");
			}
			catch ( ClassCastException ex ) {
				ret.m_totalKilled = ( (Long)ent.getProperty("totalKilled") ).intValue();
			}
			
			SeaFightShip next = null;
			
			if( ( next = shipFromEntity( "carrier", ent ) ) != null ) {
				ret.m_playerShips.add( next );
			}
			
			if( ( next = shipFromEntity( "cruiser_1", ent ) ) != null ) {
				ret.m_playerShips.add( next );
			}
	
			if( ( next = shipFromEntity( "cruiser_2", ent ) ) != null ) {
				ret.m_playerShips.add( next );
			}
			
			if( ( next = shipFromEntity( "destroyer_1", ent ) ) != null ) {
				ret.m_playerShips.add( next );
			}
			
			if( ( next = shipFromEntity( "destroyer_2", ent ) ) != null ) {
				ret.m_playerShips.add( next );
			}
			
			if( ( next = shipFromEntity( "destroyer_3", ent ) ) != null ) {
				ret.m_playerShips.add( next );
			}
	
			if( ( next = shipFromEntity( "boat_1", ent ) ) != null ) {
				ret.m_playerShips.add( next );
			}
			
			if( ( next = shipFromEntity( "boat_2", ent ) ) != null ) {
				ret.m_playerShips.add( next );
			}
			
			if( ( next = shipFromEntity( "boat_3", ent ) ) != null ) {
				ret.m_playerShips.add( next );
			}
			
			if( ( next = shipFromEntity( "boat_4", ent ) ) != null ) {
				ret.m_playerShips.add( next );
			}
		}
		
		return ret;
	}

	public int getStarted() {
		return m_nstartedGames;
	}
	
	public int getFinished() {
		return m_nfinishedGames;
	}
	
	public String toString() {
		HashMap<String, String> ret = new HashMap<String, String>();

		SeaFightProtocol.addString( ret, "name", m_name );
		SeaFightProtocol.addBoolean( ret, "online", m_online );
		SeaFightProtocol.addBoolean( ret, "banned", m_banned );
		SeaFightProtocol.addInt( ret, "nstarted", m_nstartedGames );
		SeaFightProtocol.addInt( ret, "nfinished", m_nfinishedGames );
		
		return SeaFightServer.responseToJson(ret);
	}

	public void unbahn() {
		m_banned = false;
	}
}
