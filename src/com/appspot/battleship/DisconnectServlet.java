package com.appspot.battleship;

import java.io.IOException;
import java.util.logging.Logger;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.appengine.api.channel.ChannelPresence;
import com.google.appengine.api.channel.ChannelService;
import com.google.appengine.api.channel.ChannelServiceFactory;
import com.google.appengine.api.users.User;
import com.google.appengine.api.users.UserService;
import com.google.appengine.api.users.UserServiceFactory;

public class DisconnectServlet extends HttpServlet {	
	private static final Logger log = Logger.getLogger(SeaFightServer.class.getName());
	
	@Override
    public void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws IOException {
    	/*UserService userService = UserServiceFactory.getUserService();
        User user = userService.getCurrentUser();

        if (user != null) {
        	log.info( "User " + user.getNickname() + " disconnected." );
        } */
		
		ChannelService channelService = ChannelServiceFactory.getChannelService();
		ChannelPresence presence = channelService.parsePresence(req);
		String clientId = presence.clientId();
		
		log.info( "User " + clientId + " disconnected." );
		
		SeaFightServer.channelDisconnected( clientId );
    }
}
