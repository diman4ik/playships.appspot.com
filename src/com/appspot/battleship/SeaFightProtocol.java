package com.appspot.battleship;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.appengine.api.channel.ChannelMessage;
import com.google.appengine.api.channel.ChannelService;
import com.google.appengine.api.channel.ChannelServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.KeyFactory;

public class SeaFightProtocol {

	public enum clientPackets
	{
		update(1),
        invite(2),
        myMove(3),
        spectator(4),
        am_ready(5),
        upd_game_state(6),
        invite_answer(7),
        leave(8),
        message(9),
        connectMe(10),
        player_list_update(11),
        enter(12),
        stop_game(13),
        request_token(14);

        private clientPackets( int value )
        {
			m_value = (byte)value;
        }

		public byte value()
		{
			return m_value;
		}

		private byte m_value = 0;
	}

	public enum serverPackets
	{
		playerEntered(1),
	    youInvited(2),
	    yourInviteAnswer(3),
	    gameStart(4),
	    yourTurn(5),
	    moveResult(6),
	    gameOver(7),
	    playerLeaved(8),
	    message(9),
	    opponentMove(10),
	    playerlistUpdate(11),
	    playerToken(12),
	    privateMessage(13),
        messagesTail(14),
        fieldBad(15),
        gameCancelled(16),
        playerStatusChanged(17),
        serverInfo(18), 
        newToken(19),
        overcrowded(20), 
        youBanned(21);
		
		private serverPackets( int value )
		{
			m_value = (byte)value;
		}

		public byte value()
		{
			return m_value;
		}

		private byte m_value = 0;
	}
	
	private static final Logger log = Logger.getLogger(SeaFightProtocol.class.getName());

	static void addType( Map resp, serverPackets type ) {
		resp.put( "type", Byte.toString( type.value() ) );
	}

	static void addString( Map resp, String key, String string ) {
		resp.put( key, "'" + string + "'" );
	}

	static void addBoolean( Map resp, String key, boolean value ) {
		if( value )
			resp.put( key, "true" );
		else
			resp.put( key, "false" );
	}

	static void addInt( Map resp, String key, int value ) {
		resp.put( key, Integer.toString( value ) );
	}

	static String getParam( Map pack, String param_name ) {
		return ( (String[] ) pack.get( param_name ) )[0];
	}

	public static void processInvitePack( String player_name, Map pack ) {
		String whom = ( (String[] ) pack.get( "name" ) )[0];
		SeaFightPlayer whom_invited = SeaFightServer.getPlayer( whom );
		
		if( whom_invited.inGame() )
			return;
		
		String user_id = whom_invited.getUserId();
		
		ChannelService channelService = ChannelServiceFactory.getChannelService();
		Map ans = assembelInvitePack(player_name);
		channelService.sendMessage(new ChannelMessage( user_id, SeaFightServer.responseToJson(ans) ));
	}

	public static void processInviteAnswerPack( String player_name, Map pack )
	{
		String whose = ( (String[] ) pack.get( "name" ) )[0];

		boolean desision = Boolean.parseBoolean( ( (String[] ) pack.get( "desision" ) )[0] );
		
		if( desision ) {
			// Invite accepted, start game now
			if( SeaFightServer.getPlayer(whose).inGame() ) {
				Map ans = assembleInfoPacket( "Somebody was faster than you." );
				SeaFightServer.sendChannelMessageToUser( player_name, SeaFightServer.responseToJson(ans) );
				return;	// Somebody was faster
			}
			
			SeaFightGame gm = SeaFightServer.createGame( player_name, whose );
			
			SeaFightServer.changePlayerStatus( player_name, false );
			SeaFightServer.changePlayerStatus( whose, false );
			
			Entity ent = gm.getEntity();
			String ans = SeaFightServer.responseToJson( assembleGameStartPack( player_name, whose, KeyFactory.keyToString( ent.getKey() ) ) );
			
			SeaFightPlayer player1 = SeaFightServer.getPlayer(player_name);
			SeaFightPlayer player2 = SeaFightServer.getPlayer(whose);
			
			player1.gameStarted();
			player2.gameStarted();
			
			String userid1 = player1.getUserId();
			String userid2 = player2.getUserId();
			
			ChannelService channelService = ChannelServiceFactory.getChannelService();
			channelService.sendMessage( new ChannelMessage( userid1, ans ) );
			channelService.sendMessage( new ChannelMessage( userid2, ans ) );
		}
	}

	public static void processMyMovePack( String player_name, Map pack ) {		
		// Player made his move
		int x = Integer.parseInt( getParam( pack, "x" ) );
		int y = Integer.parseInt( getParam( pack, "y" ) );
		
		String gameKey = getParam( pack, "gameKey" );
		SeaFightGame gm = SeaFightServer.getGame( gameKey );
		
		if( gm == null )
			return;
		
		if( !gm.isPlayersTurn( player_name ) ) {
			log.warning( "Player " + player_name + " tries to strike not in his turn." );
			return;
		}
		
		SeaFightGame.moveRes res = gm.newMove( player_name, x, y );
		
		Map resp = assembleYourMoveResPack( res.value(), gm.isPlayersTurn( player_name ) );
		SeaFightServer.sendChannelMessageToUser( player_name, SeaFightServer.responseToJson( resp ) );
			
		if( res != SeaFightGame.moveRes.illegal ) {		
			// Give other player your move
			String opponent_name = gm.getOpponentName( player_name );
			
			resp = assembleOpponentMovePacket( x, y, gm.isPlayersTurn( opponent_name ) );
			SeaFightServer.sendChannelMessageToUser( opponent_name, SeaFightServer.responseToJson( resp ) );
			
			if( gm.gameOver() ) {
				SeaFightPlayer player = SeaFightServer.getPlayer( player_name );
				SeaFightPlayer opponent = SeaFightServer.getPlayer( opponent_name );
				
				// Send game over
				resp = assembleGameOverPacket( player_name, opponent.getField() );
				SeaFightServer.sendChannelMessageToUser( player, SeaFightServer.responseToJson( resp ) );
				
				resp = assembleGameOverPacket( opponent_name, player.getField() );
				SeaFightServer.sendChannelMessageToUser( opponent, SeaFightServer.responseToJson( resp ) );
				
				SeaFightServer.changePlayerStatus( player, true );
				SeaFightServer.changePlayerStatus( opponent, true );
				player.gameFinished();
				opponent.gameFinished();
				SeaFightServer.removeGame( gm );
				return;
			}	
			
			SeaFightServer.saveGame( gm );
		}
		else {
			log.warning( "Player " + player_name + " made wrong move." );
		}
	}

	public static void processAmReadyPack( String player_name, Map pack ) {
		String field = getParam( pack, "field" );
		String gameKey = getParam( pack, "gameKey" );
		
		if( !SeaFightGame.checkField( field, false ) ) {
			// Error
			log.warning( "Player " + player_name + " submitted wrong field." );
			String mess = SeaFightServer.responseToJson( assembleFieldBadPacket() );
			SeaFightServer.sendChannelMessageToUser( player_name, mess );
			return;			
		}

		SeaFightServer.setPlayerField( gameKey, player_name, field );
		
		SeaFightGame gm = SeaFightServer.getGame( gameKey );
		if( gm.gameReady() ) {
			// You can start and make your move
			String who = gm.whoseTurn();
			String mess = SeaFightServer.responseToJson( assembleYoutTurnPacket() );
			SeaFightServer.sendChannelMessageToUser( who, mess );
		}
		else
			log.info( "Game not ready " + player_name );
	}
	
	public static void processPrivateMessPacket(String user_name, Map pack) {
        String adresat = getParam( pack, "adresat" );
        String message = getParam( pack, "message" );

        SeaFightServer.sendChannelMessageToUser( adresat, 
                        SeaFightServer.responseToJson( assemblePrivateMessagePacket( user_name, message ) ) );
	}
	
	public static void processGameStopPacket( String user_name, Map pack ) {
		// Send message to oponent that user has cancelled game
		String gameKey = getParam( pack, "gameKey" );
		SeaFightGame gm = SeaFightServer.getGame( gameKey );

		String opponent = gm.getOpponentName( user_name ); 

        SeaFightServer.sendChannelMessageToUser( opponent, 
                SeaFightServer.responseToJson( assembleGameCanceledPacket( user_name ) ) );
        
        SeaFightServer.changePlayerStatus( user_name, true );
        SeaFightServer.changePlayerStatus( opponent, true );
		
		SeaFightServer.removeGame(gm);
	}

	public static Map assembleTokenPacket( String name, String key, String token ) {
		HashMap<String, String> ret = new HashMap<String, String>();
		
		addType( ret, SeaFightProtocol.serverPackets.playerToken );
		addString( ret, "token", token );
		addString( ret, "playerList", SeaFightServer.constructPlayerListMessage() );
		addString( ret, "playerKey", key );
		addString( ret, "name", name );
		addString( ret, "tail", messagesTail() );

		return ret;
	}
	
	public static Map assemblePlayerListPacket() {
		HashMap<String, String> ret = new HashMap<String, String>();
		
		addType( ret, SeaFightProtocol.serverPackets.playerlistUpdate );
		addString( ret, "playerList", SeaFightServer.constructPlayerListMessage() );
		
		return ret;
	}
	
	private static Map assembleGameStartPack( String player_name, String whose,
			String key ) {
		HashMap<String, String> ret = new HashMap<String, String>();
		
		addType( ret, SeaFightProtocol.serverPackets.gameStart);
		addString( ret, "player1", whose );
		addString( ret, "player2", player_name );
		addString( ret, "gameKey", key );
		
		return ret;
	}
	
	private static Map assembleYoutTurnPacket() {
		HashMap<String, String> ret = new HashMap<String, String>();
		addType( ret, SeaFightProtocol.serverPackets.yourTurn );
		return ret;
	}
	
	private static Map assembleGameOverPacket( String player_name, String opponentField ) {
		HashMap<String, String> ret = new HashMap<String, String>();
		addType( ret, SeaFightProtocol.serverPackets.gameOver );
		addString( ret, "opponent_field", opponentField );
		return ret;
	}
	
	private static Map assembelInvitePack( String player_name ) {
		HashMap<String, String> ret = new HashMap<String, String>();
		addType( ret, SeaFightProtocol.serverPackets.youInvited );
		addString( ret, "inviter", player_name );
		return ret;
	}
	
	public static Map assembleYourMoveResPack( int res, boolean your_turn ) {
		HashMap<String, String> ret = new HashMap<String, String>();

		addType( ret, SeaFightProtocol.serverPackets.moveResult );
		addInt( ret, "result", res );
		addBoolean( ret, "yourTurn", your_turn );

		return ret;
	}
	
	private static Map assembleOpponentMovePacket( int x, int y, boolean your_turn ) {
		HashMap<String, String> ret = new HashMap<String, String>();

		addType( ret, SeaFightProtocol.serverPackets.opponentMove );
		addInt( ret, "x", x );
		addInt( ret, "y", y );
		addBoolean( ret, "yourTurn", your_turn );

		return ret;
	}

	public static Map assemblePlayerEnteredPacket( String name ) {
		HashMap<String, String> ret = new HashMap<String, String>();

		addType( ret, SeaFightProtocol.serverPackets.playerEntered );
		addString( ret, "name", name );

		return ret;
	}

	public static Map assemblePlayerLeavedPacket( String name ) {
		HashMap<String, String> ret = new HashMap<String, String>();

		addType( ret, SeaFightProtocol.serverPackets.playerLeaved );
		addString( ret, "name", name );

		return ret;
	}

	public static Map assembleChatMessage( String user_name, String playerMessage ) {
		HashMap<String, String> ret = new HashMap<String, String>();

		addType( ret, SeaFightProtocol.serverPackets.message );
		addString( ret, "name", user_name );
		addString( ret, "message", playerMessage );
		addString( ret, "from", user_name );

		return ret;
	}
	
	public static String messagesTail() {
		List<String> messages = ChatServlet.getTail();
		
		if( messages == null )
			return "";
		
		String text = "";
	    
		for( String mess : messages ) {
            text += mess + ";.;";
		}
		
		if( text.length() > 0 )
			text = text.substring( 0, text.length() - 3 );
		
		return text;
	}
	
	public static Map assembleMessagesTailPacket( String userName ) {
		String text = messagesTail(); 
    
		HashMap<String, String> ret = new HashMap<String, String>();
    
		addType( ret, SeaFightProtocol.serverPackets.messagesTail );
		addString( ret, "messages", text );
    
		return ret;
	}

	public static Map assemblePrivateMessagePacket(String userName,
            String playerMessage) {
		HashMap<String, String> ret = new HashMap<String, String>();
    
    	addType( ret, SeaFightProtocol.serverPackets.privateMessage );
    	addString( ret, "name", userName );
    	addString( ret, "message", playerMessage );
    
    	return ret;
	}
	
	public static Map assembleFieldBadPacket() {
		HashMap<String, String> ret = new HashMap<String, String>();
		
		addType( ret, SeaFightProtocol.serverPackets.fieldBad );
		return ret;
	}
	
	public static Map assembleGameCanceledPacket( String user_name ) {
		HashMap<String, String> ret = new HashMap<String, String>();
		
		addType( ret, SeaFightProtocol.serverPackets.gameCancelled );
		addString( ret, "name", user_name );
		
		return ret;	
	}
	
	public static Map assemblePlayerStatusChanged( String name, boolean ready ) {
		HashMap<String, String> ret = new HashMap<String, String>();
		
		addType( ret, SeaFightProtocol.serverPackets.playerStatusChanged );
		addBoolean( ret, "ready", ready );
		
		addString( ret, "name", name );
		
		return ret;			
	}

	public static Map assembleNewTokenPacket( String token ) {
		HashMap<String, String> ret = new HashMap<String, String>();
		
		addType( ret, SeaFightProtocol.serverPackets.newToken );
		addString( ret, "token", token );
		
		return ret;
	}
	
	public static Map assembleInfoPacket( String info ) {
		HashMap<String, String> ret = new HashMap<String, String>();
		
		addType( ret, SeaFightProtocol.serverPackets.serverInfo );
		addString( ret, "info", info );
		
		return ret;		
	}

	public static Map assembleOvercrowdedPack() {
		HashMap<String, String> ret = new HashMap<String, String>();
		addType( ret, SeaFightProtocol.serverPackets.overcrowded );
		return ret;
	}

	public static Map assembleBannedPacket() {
		HashMap<String, String> ret = new HashMap<String, String>();
		addType( ret, SeaFightProtocol.serverPackets.youBanned );
		return ret;
	}
}
