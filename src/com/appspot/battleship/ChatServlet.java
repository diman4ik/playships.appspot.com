package com.appspot.battleship;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.appengine.api.users.*;


public class ChatServlet extends HttpServlet {

	private static String messages_key = "chat_messages_key";
	
	public void doPost(HttpServletRequest req, HttpServletResponse resp)
		throws IOException {
		
        UserService userService = UserServiceFactory.getUserService();
        User user = userService.getCurrentUser();
        
        String type = req.getParameter("type");
        String playerMessage = req.getParameter("text");
        String userName = user.getNickname();
        
        if( type.compareTo( "common" ) == 0 ) {
                addMessageToCache( userName, playerMessage );
                Map mess = SeaFightProtocol.assembleChatMessage( userName, playerMessage );
                SeaFightServer.sendChannelMessageToAll( SeaFightServer.responseToJson( mess ) );
        }
        else if( type.compareTo( "private" ) == 0 ) {
                String adress = req.getParameter("adress");
                String message = SeaFightServer.responseToJson(
                                SeaFightProtocol.assemblePrivateMessagePacket( userName,  playerMessage ) );
                
                SeaFightServer.sendChannelMessageToUser( adress, message );
        }
        else if( type.compareTo( "get_tail" ) == 0 ) {
                // Send tail of common messages to requester
                List<String> messages = getTail();
                
                String message = SeaFightServer.responseToJson(
                                SeaFightProtocol.assembleMessagesTailPacket( userName ) );
                
                SeaFightServer.sendChannelMessageToUser(userName, message);
        }
	}

	public static List getTail() {
        return Util.getCacheList( messages_key );
	}

	private static void addMessageToCache( String user, String text ) {
        
        String message = user + ":" + text;
        
        List messages = Util.getCacheList( messages_key );

        if( messages == null )
        	messages = new LinkedList<String>();

        if( messages.size() > 50 )
            messages.remove( 0 );
        
        messages.add( message );
        Util.setCacheList( messages_key, messages );
	}
}
