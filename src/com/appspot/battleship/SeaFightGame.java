package com.appspot.battleship;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.appengine.api.datastore.EmbeddedEntity;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.Key;


public class SeaFightGame {
	
	enum moveRes
	{
		miss(0),
		hit(1),
		boat_killed(2),
		destroyer_killed(3),
		cruiser_killed(4),
		carrier_killed(5),
		illegal(-1);

		private final int val;
		
		public static Boolean gameover = false;

		moveRes(int val)
		{
			this.val = val;
		}

		public int value()
		{
			return val;
		}
	}
	
	private String m_key = ""; // Unique game code

	private String m_player1 = "";
	private String m_player2 = "";
	private String m_whoseTurn = "";

	private boolean m_player1Ready = false;
	private boolean m_player2Ready = false;
	private boolean m_gameOver = false;
	
	private static final Logger log = Logger.getLogger(SeaFightGame.class.getName());
	

	SeaFightGame( String player1, String player2 ) {
		m_player1 = player1;
		m_player2 = player2;
		
		m_whoseTurn = m_player1;
		
		if( m_key.length() == 0 ) {
			Random rn = new Random();
			m_key = m_player1 + "_" + m_player2 + "_" + rn.nextInt(5);
		}
	}
	
	public void setPlayerReady( String player ) {
		if( player.equals( m_player1 ) ) {
			m_player1Ready = true;
		}
		else if( player.equals(m_player2 ) ) {
			m_player2Ready = true;
		}
	}
	
	public moveRes newMove( String who, int x, int y ) {
		
		moveRes res;
		Boolean killed = false;
		
		int index = FIELD_SIZE*y + x;

		if( ( index < 0 ) || ( index >= FIELD_SIZE*FIELD_SIZE ) )
		{
			log.warning( "Move index " + index + " not valid " + who );
			res = moveRes.illegal;
			return res;
		}
		
		SeaFightPlayer player = SeaFightServer.getPlayer( getOpponentName(who) );
		res = player.getMoveRes( index );
		SeaFightServer.savePlayer(player);
 		
		if( res.gameover ) {
			m_gameOver = true;
		}
		else if( res == moveRes.miss ) {
 			m_whoseTurn = getOpponentName( who );
		}
 		
 		return res;
	}
	
	public boolean gameReady()
	{
		return ( m_player1Ready && m_player2Ready );
	}

	public boolean isPlayersTurn( String name ) {
		if( !gameReady() )
			return false;
		
		if( gameOver() )
			return false;

		return ( m_whoseTurn.equals( name ) );
	}

	public String getOpponentName( String for_whom ) {
		if( for_whom.equals( m_player1 ) )
		{
			return m_player2;
		}
		else if( for_whom.equals( m_player2 ) )
		{
			return m_player1;
		}

		return null;
	}
	
	public String getKey() {
		return m_key;
	}

	public boolean gameOver() {
		return m_gameOver; 
	}
	
	public String whoseTurn() {
		return m_whoseTurn;
	}

	public void merge( SeaFightGame game2 ) {
		if( !m_player1Ready ) {
			if( game2.m_player1Ready ) {
				m_player1Ready = true;
			}
		}
		
		if( !m_player2Ready ) {
			if( game2.m_player2Ready ) {
				m_player2Ready = true;
			}			
		}
	}
	
	enum fieldType
	{
		sea('~'),
		ship('o'),
		hit('x'),
		miss('.');

		private final char val;

		fieldType(char val)
		{
			this.val = val;
		}

		public char value()
		{
			return val;
		}
	}
	
	static final int FIELD_SIZE = 10;
	static final int TOTAL_SHIPS = 10;
	static final int FIELD_ARRAY_SIZE = FIELD_SIZE*FIELD_SIZE;

	static final int TOTAL_4_FIELD = 1;
	static final int TOTAL_3_FIELD = 2;
	static final int TOTAL_2_FIELD = 3;
	static final int TOTAL_1_FIELD = 4;
	
	public static boolean isCellValid( char cell, boolean inGame )
	{
		char sea = fieldType.sea.value();
		char ship = fieldType.ship.value();

		if( ( cell == sea ) ||
			( cell == ship ) )
			return true;
		return false;
	}
	
	public static boolean checkField( String field, boolean inGame )
	{
		for( int i = 0; i < FIELD_ARRAY_SIZE; i++ )
		{
			if( !isCellValid( field.charAt(i), inGame ) )
				return false;
		}
		
		List<SeaFightShip> ships = findFieldShips( field );
		
		int current4Field = 0;
		int current3Field = 0;
		int current2Field = 0;
		int current1Field = 0;

		if( ships.size() != TOTAL_SHIPS )
		{
			log.warning( "Not all ships positioned by the user." );
			return false;
		}
		else
		{
			for( SeaFightShip ship : ships )
			{
				int length = ship.m_length;

				if( length == 1  )
					current1Field++;
				else if( length == 2 )
					current2Field++;
				else if( length == 3 )
					current3Field++;
				else if( length == 4 )
					current4Field++;
			}

			if( ( current1Field == TOTAL_1_FIELD ) &&
				( current2Field == TOTAL_2_FIELD ) &&
				( current3Field == TOTAL_3_FIELD ) &&
				( current4Field == TOTAL_4_FIELD ) )
				return true;
			return false;
		}
	}
	
	public static List<SeaFightShip> findFieldShips( String field ) {
		List<SeaFightShip> ships = new ArrayList<SeaFightShip>();
				
		for( int i = 0; i < FIELD_ARRAY_SIZE; i++ )
		{
			if( field.charAt(i) == fieldType.ship.value() )
			{
				boolean found = false;

				for( int k = 0; k < ships.size(); k++ )
				{
					SeaFightShip sh = ships.get(k);

					if( sh.canAdd( i ) )
					{
						sh.m_botRightX = i%FIELD_SIZE;
						sh.m_botRightY = i/FIELD_SIZE;
						sh.m_length++;
						found = true;
					}
				}

				if(found)
					continue;

				if( i % FIELD_SIZE != 0 ) {
					if( ( (i - 1) > 0 ) && ( field.charAt( i - 1 ) == fieldType.ship.value() ) ) {
						ships.clear();
						return ships;
					}
				}
				if( ( (i - FIELD_SIZE) > 0 ) &&
					( field.charAt( i - FIELD_SIZE ) == fieldType.ship.value() ) ) {
					ships.clear();
					return ships;
				}

				if( ( (i+ 1) < FIELD_ARRAY_SIZE ) &&
					( field.charAt( i + 1 ) == fieldType.ship.value() ) )
				{
					SeaFightShip sh = new SeaFightShip();
					sh.m_direction = SeaFightShip.direction.horizontal;
					sh.m_botRightX = i%FIELD_SIZE;
					sh.m_botRightY = i/FIELD_SIZE;
					sh.m_cell = i;
					sh.m_length = 1;
					ships.add(sh);
				}
				else if( ( (i+ FIELD_SIZE) < FIELD_ARRAY_SIZE ) &&
						 ( field.charAt( i + FIELD_SIZE ) == fieldType.ship.value() ) )
				{
					SeaFightShip sh = new SeaFightShip();
					sh.m_direction = SeaFightShip.direction.vertical;
					sh.m_botRightX = i%FIELD_SIZE;
					sh.m_botRightY = i/FIELD_SIZE;
					sh.m_cell = i;
					sh.m_length = 1;
					ships.add(sh);
				}
				else
				{
					SeaFightShip sh = new SeaFightShip();
					sh.m_direction = SeaFightShip.direction.none;
					sh.m_botRightX = i%FIELD_SIZE;
					sh.m_botRightY = i/FIELD_SIZE;
					sh.m_length = 1;
					sh.m_cell = i;
					ships.add(sh);
				}
			}
		}
		
		return ships;
	}
	
	public Entity getEntity() {
		Entity ret = new Entity("Game", m_key);
		ret.setProperty( "player1", m_player1 );
		ret.setProperty( "player2", m_player2 );
		ret.setProperty( "key", m_key );
		ret.setProperty( "player1Ready", m_player1Ready );
		ret.setProperty( "player2Ready", m_player2Ready );
		ret.setProperty( "whoseTurn", m_whoseTurn );
		ret.setProperty( "gameover", m_gameOver );
		
		return  ret;
	}
	
	public static SeaFightGame fromEntity( Entity ent ) {
		
		String player1 = (String )ent.getProperty( "player1" );
		String player2 = (String )ent.getProperty( "player2" );
		
		SeaFightGame ret = new SeaFightGame( player1, player2 );
		
		ret.m_key = (String )ent.getProperty( "key" );
		ret.m_player1Ready = (Boolean )ent.getProperty( "player1Ready" );
		ret.m_player2Ready = (Boolean )ent.getProperty( "player2Ready" );
		ret.m_whoseTurn = (String )ent.getProperty( "whoseTurn" );
		ret.m_gameOver = (Boolean)ent.getProperty("gameover");
				
		return ret;
	}
}
