
test('BattleshipView.shipBoundingCells()', function () {
	{
		var ship = {
			cell_x : 0,
			cell_y : 0, 
			length : 4,
			rotated : true
		};
		
		var ret = BattleshipView.shipBoundingCells( ship );
		
		ok( ret.length === 6, "0, 0, rotated, length 4" );	
	}
	
	{
		var ship = {
			cell_x : 2,
			cell_y : 3, 
			length : 1,
			rotated : false
		};
			
		var ret = BattleshipView.shipBoundingCells( ship );
			
		ok( ret.length === 8, "2, 2, length 1" );
	}
	
	{
		var ship = {
				cell_x : 0,
				cell_y : 1, 
				length : 3,
				rotated : true
		};
				
		var ret = BattleshipView.shipBoundingCells( ship );
				
		ok( ret.length === 7, "1, 1, length 3" );
	}
});

test('BattleshipView.addExplosionsAroundShip()', function () {	
	{
		var misses = "2,3,15,16,43,47,76,";
		sessionStorage.setItem( "player_miss", misses );
		
		var ship = {
				cell_x : 6,
				cell_y : 2, 
				length : 4,
				rotated : true
		};
		
		BattleshipView.addExplosionsAroundShip(ship);
		
		var misses_after = sessionStorage.getItem( "player_miss" ).split(',');
		
		ok( misses_after.length == 19, "addexplosionsAroundShip 1");
	}
});

test('BattleshipView.findLastInChain()', function () {
	{
		var player_hits = new Array();
		
		player_hits.push( "2" );
		player_hits.push( "3" );
		
		var ret = BattleshipView.findLastInChain( player_hits, 3 );
		
		ok( ret === 2, "findLastInChain" );
	}
	
	{
		var player_hits = new Array();
		
		player_hits.push( "8" );
		player_hits.push( "9" );
		player_hits.push( "10" );
		player_hits.push( "11" );
		
		var ret = BattleshipView.findLastInChain( player_hits, 10 );
		
		ok( ret === 10, "findLastInChain" );
	}
	
	{
		var player_hits = new Array();
		
		player_hits.push( "0" );
		player_hits.push( "1" );
		player_hits.push( "2" );
		player_hits.push( "3" );
		
		var ret = BattleshipView.findLastInChain( player_hits, 3 );
		
		ok( ret === 0, "findLastInChain" );
	}
});
