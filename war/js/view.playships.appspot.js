function BattleshipView(width, height, canvas_id) {
	BattleshipView.canvas = oCanvas.create({
		canvas : '#game_field',
		background: ""
	});

	var margin = 20;

	var width = BattleshipView.canvas.width / 2 - margin * 2;
	var height = BattleshipView.canvas.height - margin * 2;

	BattleshipView.fieldSize;

	if (width > height)
		BattleshipView.fieldSize = height;
	else
		BattleshipView.fieldSize = width;

	BattleshipView.cellSize = BattleshipView.fieldSize
			/ BattleshipView.fieldSizeCells;

	BattleshipView.playerField = BattleshipView.canvas.display.rectangle({
		x : BattleshipView.canvas.width / 4,
		y : BattleshipView.canvas.height / 2,
		origin : {
			x : "center",
			y : "center"
		},
		width : BattleshipView.fieldSize,
		height : BattleshipView.fieldSize,
		stroke : "2px #804B05",
		join : "round"
	});

	BattleshipView.opponentField = BattleshipView.canvas.display.rectangle({
		x : (BattleshipView.canvas.width / 4) * 3,
		y : BattleshipView.canvas.height / 2,
		origin : {
			x : "center",
			y : "center"
		},
		width : BattleshipView.fieldSize,
		height : BattleshipView.fieldSize,
		stroke : "2px " + BattleshipView.strokeColor,
		join : "round"
	});

	BattleshipView.canvas.addChild(BattleshipView.playerField);
	BattleshipView.canvas.addChild(BattleshipView.opponentField);

	BattleshipView.drawGrid(BattleshipView.canvas, BattleshipView.playerField);
	BattleshipView
			.drawGrid(BattleshipView.canvas, BattleshipView.opponentField);

	BattleshipView.shipPlacementMode = false;

	BattleshipView.placementDialog = $('<div id="placementDialog"></div>')
			.html(
				"<p id='placement_text'></p>\
				<canvas id='place_dialog_canvas' width=600 height=400></canvas>\
				<input type='button' class='placement_dialog_ok' value='OK' \
				onclick='BattleshipView.placementOK()'></input>")
			.dialog({
				autoOpen : false,
				title : 'Place your ships',
				width : 700,
				height : 510,
				draggable : false,
				resizable : false,
				position : top,
				modal : true,
				create: function(event, ui)  
				{
					$(this).parents(".ui-dialog").css("border", 0);
					$(this).parents(".ui-dialog").css("background", "transparent");
					
					$(this).parents(".ui-dialog").css( "border-style", "solid");
					$(this).parents(".ui-dialog").css( "border-width", "17px");
					$(this).parents(".ui-dialog").css( "-moz-border-image", "url(images/map_slice_3.png) 34 repeat" );
					$(this).parents(".ui-dialog").css( "-webkit-border-image", "url(images/map_slice_3.png) 34 repeat" );
					$(this).parents(".ui-dialog").css( "-o-border-image", "url(images/map_slice_3.png) 34 repeat" );
					$(this).parents(".ui-dialog").css( "border-image", "url(images/map_slice_3.png) 34 fill repeat" );
				}
			});
	
	BattleshipView.placementCanvas = oCanvas.create({
		canvas : '#place_dialog_canvas'
	});

	BattleshipView.placementField = BattleshipView.placementCanvas.display
			.rectangle({
				x : BattleshipView.fieldSize / 2 + 15,
				y : BattleshipView.fieldSize / 2 + 10,
				origin : {
					x : "center",
					y : "center"
				},
				width : BattleshipView.fieldSize,
				height : BattleshipView.fieldSize,
				stroke : "2px " + BattleshipView.strokeColor,
				join : "round"
			});

	BattleshipView.placementCanvas.addChild(BattleshipView.placementField);
	BattleshipView.drawGrid(BattleshipView.placementCanvas,
			BattleshipView.placementField);

	{
		// Ships drawn on placement field
		BattleshipView.placedShips = new Array();

		var nextShipLength = 1;
		var line = 0;
		var line_width = 50;

		for ( var j = 0; j < BattleshipView.shipCount;) {
			for ( var i = 0; i < BattleshipView
					.getNumberOfShips(nextShipLength); i++) {

				var next_x = BattleshipView.fieldSize + 50
						+ (BattleshipView.cellSize * nextShipLength + 10) * i;
				var next_y = 25 + line * line_width;

				if (next_x + BattleshipView.cellSize * nextShipLength > BattleshipView.placementCanvas.width) {
					next_x = BattleshipView.fieldSize + 50;
					next_y += line_width;
					line++;
				}

				var nextShip = BattleshipView.placementCanvas.display
						.image({
							x : next_x,
							y : next_y,
							origin : {
								x : BattleshipView.cellSize / 2,
								y : BattleshipView.cellSize / 2
							},
							width : BattleshipView.cellSize * nextShipLength,
							height : BattleshipView.cellSize,
							image:"../images/ship" + nextShipLength + ".png",
							length : nextShipLength,
							cell_x : -1,
							cell_y : -1,
							rotated : false,
							initial_x : next_x,
							initial_y : next_y,
							ship_id : i + '_' + j
						});

				BattleshipView.placementCanvas.addChild(nextShip);
				nextShip.dragAndDrop();
				nextShip.bind('mousedown', BattleshipView.mouseDown);
				nextShip.bind('mouseup', BattleshipView.mouseUp);

				BattleshipView.placedShips.push(nextShip);
				j++;

			}
			nextShipLength = BattleshipView.getNextShipLength(nextShipLength,
					"up");
			line++;
		}
	}

	{
		var nextShipLength = 1;
		// Ships drawn on regular player field
		BattleshipView.playerShips = new Array();
		BattleshipView.opponentShips = new Array();

		for ( var j = 0; j < BattleshipView.shipCount;) {
			for ( var i = 0; i < BattleshipView
					.getNumberOfShips(nextShipLength); i++) {

				var nextShip = BattleshipView.canvas.display.image({
					x : 0,
					y : 0,
					origin : {
						x : BattleshipView.cellSize / 2,
						y : BattleshipView.cellSize / 2
					},
					width : BattleshipView.cellSize * nextShipLength,
					height : BattleshipView.cellSize,
					image:"../images/ship" + nextShipLength + ".png",
					length : nextShipLength,
					cell_x : -1,
					cell_y : -1,
					rotated : false,
					ship_id : i + '_' + j
				});

				BattleshipView.playerShips.push(nextShip);
				j++;
			}

			nextShipLength = BattleshipView.getNextShipLength(nextShipLength,
					"up");
		}
	}

	BattleshipView.placementCanvas.bind('keydown', BattleshipView.keypress);

	BattleshipView.playerField.bind('mousedown', BattleshipView.mouseClick);
	BattleshipView.opponentField.bind('mousedown', BattleshipView.mouseClick);

	BattleshipView.yourTurnDialog = $("<div></div>")
		.html(
				"<p class='your_turn_mess'>It's your turn.</p>")
		.dialog({
			autoOpen : false,
			title : 'Your turn',
			draggable : false,
			resizable : false,
			width : 120,
			height : 120,
			modal : false,
			create: function(event, ui)  
			{
				$(this).parents(".ui-dialog").css("Background-color", "gray");
				$(this).parents(".ui-dialog").css("opacity", 0.7);
			}
		});
	
	BattleshipView.lastMove = null;

	if (sessionStorage.getItem('placementMode') === 'true') {
		var name = sessionStorage.getItem('opponent_name');
		BattleshipView.startPlacementMode(name);
	}

	if (sessionStorage.getItem('gameMode') === 'true') {
		BattleshipView.restorePlayerShips();

		for ( var i = 0; i < BattleshipView.playerShips.length; i++) {
			BattleshipView.canvas.addChild(BattleshipView.playerShips[i]);
		}

		BattleshipView.restoreExplosions();

		BattleshipView.canvas.redraw();
	}	
	$(".ui-dialog-titlebar").hide();
}

BattleshipView.fieldSizeCells = 10;
BattleshipView.shipCount = 10;

BattleshipView.explosions = new Array();

BattleshipView.drawGrid = function(canvas, field) {
	var cellSize = BattleshipView.cellSize;

	var start_x = field.x - field.width / 2;
	var start_y = field.y - field.height / 2;

	for ( var i = 1; i < BattleshipView.fieldSizeCells; i++) {
		var line = canvas.display.line({
			start : {
				x : start_x + cellSize * i,
				y : start_y
			},
			end : {
				x : start_x + cellSize * i,
				y : start_y + field.height
			},
			stroke : "2px " + BattleshipView.strokeColor,
		});

		canvas.addChild(line);

		var line = canvas.display.line({
			start : {
				x : start_x,
				y : start_y + cellSize * i
			},
			end : {
				x : start_x + field.width,
				y : start_y + cellSize * i
			},
			stroke : "2px " + BattleshipView.strokeColor,
		});

		canvas.addChild(line);
	}
}

BattleshipView.strokeColor = "#804B05";

BattleshipView.startPlacementMode = function(name) {
	$('#placement_text')
		.text(	"Your game with " + name + " started. Place your ships please. Press space to rotate.");

	BattleshipView.shipPlacementMode = true;

	if (!(sessionStorage.getItem('placementMode') === 'true')) {
		// Save that we are in placement mode
		sessionStorage.setItem('placementMode', true);
		BattleshipView.setPlacedShipsInitalPos();
	} else {
		// Page reload restore ship positions
		BattleshipView.restorePlacedShips();
	}

	BattleshipView.placementCanvas.redraw();
	BattleshipView.placementDialog.dialog('open');
}

BattleshipView.setPlacedShipsInitalPos = function() {
	for ( var i = 0; i < BattleshipView.placedShips.length; i++) {
		BattleshipView.placedShips[i].x = BattleshipView.placedShips[i].initial_x;
		BattleshipView.placedShips[i].y = BattleshipView.placedShips[i].initial_y;
		BattleshipView.placedShips[i].moveTo(
				BattleshipView.placedShips[i].initial_x,
				BattleshipView.placedShips[i].initial_y);
		if (BattleshipView.placedShips[i].rotated) {
			BattleshipView.placedShips[i].rotated = false;
			BattleshipView.placedShips[i].rotate(-90);
		}
		BattleshipView.placedShips[i].cell_x = -1;
		BattleshipView.placedShips[i].cell_y = -1;
		BattleshipView.placedShips[i].rotated = false;

		BattleshipView.saveShipPos(BattleshipView.placedShips[i]);
	}
}

BattleshipView.saveShipPos = function(ship) {
	var coord_object = {
		x : ship.x,
		y : ship.y,
		cell_x : ship.cell_x,
		cell_y : ship.cell_y,
		rotated : ship.rotated
	};
	sessionStorage.setItem(ship.ship_id, JSON.stringify(coord_object));
}

BattleshipView.placementOK = function() {
	// Check if any ships intersect
	var position_valid = (function() {
		for ( var i = 0; i < BattleshipView.placedShips.length; i++) {
			if ((BattleshipView.placedShips[i].cell_x == -1)
					|| (BattleshipView.placedShips[i].cell_y == -1))
				return false;
		}

		for ( var i = 0; i < BattleshipView.placedShips.length; i++) {
			for ( var j = i + 1; j < BattleshipView.placedShips.length; j++) {
				if (BattleshipView.shipsIntersect(
						BattleshipView.placedShips[i],
						BattleshipView.placedShips[j])) {
					return false;
				}
			}
		}

		return true;
	})();

	if (position_valid) {
		BattleshipView.shipPlacementMode = false;
		BattleshipView.placementDialog.dialog('close');

		BattleshipView.restorePlayerShips();

		for ( var i = 0; i < BattleshipView.playerShips.length; i++) {
			BattleshipView.canvas.addChild(BattleshipView.playerShips[i]);
		}

		BattleshipView.canvas.redraw();

		sessionStorage.setItem('placementMode', false);
		sessionStorage.setItem('gameMode', true);

		// Here we need to send the position to server
		BattleshipClient
				.sendPackage(BattleshipClient
						.createShipsPlacedPacket(BattleshipView
								.getPlayerFieldString()));
	} else {
		alert("Position BAD!");
	}
}

BattleshipView.restorePlacedShips = function() {
	for ( var i = 0; i < BattleshipView.placedShips.length; i++) {
		var item = sessionStorage
				.getItem(BattleshipView.placedShips[i].ship_id);

		if (item != null) {
			var coords = JSON.parse(item);
			BattleshipView.placedShips[i].moveTo(coords.x, coords.y);
			BattleshipView.placedShips[i].cell_x = coords.cell_x;
			BattleshipView.placedShips[i].cell_y = coords.cell_y;

			if (coords.rotated) {
				BattleshipView.placedShips[i].rotate(90);
				BattleshipView.placedShips[i].rotated = true;
			}
		}
	}
}

BattleshipView.restorePlayerShips = function() {
	for ( var i = 0; i < BattleshipView.playerShips.length; i++) {
		var item = sessionStorage
				.getItem(BattleshipView.playerShips[i].ship_id);

		if (item != null) {
			var coords = JSON.parse(item);

			BattleshipView.playerShips[i].cell_x = coords.cell_x;
			BattleshipView.playerShips[i].cell_y = coords.cell_y;

			var coord_x = BattleshipView.playerShips[i].cell_x
					* BattleshipView.cellSize + 1;
			var coord_y = BattleshipView.playerShips[i].cell_y
					* BattleshipView.cellSize + 1;

			BattleshipView.playerShips[i].moveTo(coord_x, coord_y);

			if (coords.rotated) {
				BattleshipView.playerShips[i].rotate(90);
				BattleshipView.playerShips[i].rotated = true;
			}
			else {
				BattleshipView.playerShips[i].rotated = false;
			}
		}
	}
}

BattleshipView.restoreExplosions = function() {
	for ( var i = 0; i < BattleshipView.explosions.length; i++) {
		BattleshipView.canvas.removeChild(BattleshipView.explosions[i]);
	}
	
	function addHitMiss(array, field, hit) {
		for ( var i = 0; i < array.length; i++) {
			if (array[i].length == 0)
				break;
			var coords = {
				x : Math.floor( array[i] % BattleshipView.fieldSizeCells ),
				y : Math.floor( array[i] / BattleshipView.fieldSizeCells )
			};

			if (hit == 'hit')
				BattleshipView.addHit(field, coords, false);
			else
				BattleshipView.addMiss(field, coords, false);
		}
	}

	var player_misses = "";
	
	if( ( player_misses = sessionStorage.getItem('player_miss') ) != null ) {
		player_misses = player_misses.split(',');
		addHitMiss(player_misses, BattleshipView.opponentField, 'miss');
	}
	
	var player_hits = "";

	if( ( player_hits = sessionStorage.getItem('player_hit') ) != null ) {
		player_hits = player_hits.split(',');
		addHitMiss(player_hits, BattleshipView.opponentField, 'hit');
	}
	
	var opponent_misses = "";

	if( ( opponent_misses = sessionStorage.getItem('opponent_miss') ) != null ) {
		opponent_misses = opponent_misses.split(',');
		addHitMiss(opponent_misses, BattleshipView.playerField, 'miss');
	}
	
	var opponent_hits = "";

	if( ( opponent_hits = sessionStorage.getItem('opponent_hit') ) != null ) {
		opponent_hits =	opponent_hits.split(',');
		addHitMiss(opponent_hits, BattleshipView.playerField, 'hit');
	}
	
	BattleshipView.canvas.redraw();
}

BattleshipView.addHit = function(field, coords, redraw) {
	var start_x = field.x - field.width / 2;
	var start_y = field.y - field.width / 2;

	var image = BattleshipView.canvas.display.image({
		x : start_x + (coords.x) * BattleshipView.cellSize,
		y : start_y + (coords.y) * BattleshipView.cellSize,
		origin : {
			x : 0,
			y : 0
		},
		width : BattleshipView.cellSize - 2,
		height : BattleshipView.cellSize - 2,
		image : "../images/flame8.png",
		cell_x : coords.x,
		cell_y : coords.y
	});
	BattleshipView.explosions.push(image);
	BattleshipView.canvas.addChild(image, redraw);
}

BattleshipView.addMiss = function(field, coords, redraw) {
	var start_x = field.x - field.width / 2;
	var start_y = field.y - field.width / 2;

	var image = BattleshipView.canvas.display.image({
		x : start_x + (coords.x) * BattleshipView.cellSize,
		y : start_y + (coords.y) * BattleshipView.cellSize,
		origin : {
			x : 0,
			y : 0
		},
		width : BattleshipView.cellSize - 2,
		height : BattleshipView.cellSize - 2,
		image : "../images/miss.png",
		pls_obj_type : "explosion",
		cell_x : coords.x,
		cell_y : coords.y
	});

	BattleshipView.explosions.push(image);
	BattleshipView.canvas.addChild(image, redraw);
}

BattleshipView.addKilledOpponentShip = function( length ) {
	
	var cell = BattleshipView.lastMove.x + BattleshipView.lastMove.y*10; 
	
	var player_hits = "";
	
	if( ( player_hits = sessionStorage.getItem('player_hit') ) != null ) {
		player_hits = player_hits.split(',');
	}
	
	// Check nearby player hits to see the direction
	var ship_start = cell;
	
	if( length > 1 ) {
		ship_start = BattleshipView.findLastInChain( player_hits, cell );
	}
	
	// Now we found the start, let's find direction
	var next = BattleshipView.findNextInChain( player_hits, ship_start );
	
	var direction = "horizontal";
	
	if( ( next - ship_start ) == 10 )
		direction = "vertical";
	
	var ship = BattleshipView.canvas.display.image({
		x : 0,
		y : 0,
		origin : {
			x : 0,
			y : 0
		},
		width : BattleshipView.cellSize * length,
		height : BattleshipView.cellSize,
		image:"../images/ship" + length + ".png",
		length : length,
		cell_x : Math.round( ship_start%10 ),
		cell_y : Math.floor( ship_start/10 ),
		rotated : ( direction === "vertical" )
	});
	
	BattleshipView.opponentShips.push(ship);
	BattleshipView.canvas.addChild(ship);
	
	if( ship.rotated ) {
		ship.rotate(90);
	}
	
	var coord_x = BattleshipView.opponentField.x - BattleshipView.opponentField.width/2 + 
		ship.cell_x * BattleshipView.cellSize + 1;
	
	if( ship.rotated ) {
		coord_x += BattleshipView.cellSize - 2;
	}
	
	var coord_y = BattleshipView.opponentField.y - BattleshipView.opponentField.width/2 + 
		ship.cell_y * BattleshipView.cellSize + 1;

	ship.moveTo(coord_x, coord_y);		
		
	BattleshipView.addExplosionsAroundShip( ship );

	var ship_cells = BattleshipView.shipCells( ship );
	
	for( var i = 0; i < ship_cells.length; i++ ) {
		BattleshipView.addHit( BattleshipView.opponentField, ship_cells[i], true );
	}
	
	BattleshipView.canvas.redraw();
}

BattleshipView.addExplosionsAroundShip = function( ship ) {
	var player_misses = "";
	
	var explosions = BattleshipView.shipBoundingCells(ship);
	
	if( ( player_misses = sessionStorage.getItem('player_miss') ) != null ) {
		player_misses = player_misses.split(',');
		
		for( var j = 0; j < explosions.length; j++ ) {
			var found = false;
			
			for( var i = 0; i < player_misses.length; i++ ) {
				var coords = {
					x : Math.floor( player_misses[i] % BattleshipView.fieldSizeCells ),
					y : Math.floor( player_misses[i] / BattleshipView.fieldSizeCells )
				};
				
				if( ( explosions[j].x == coords.x ) && ( explosions[j].y == coords.y ) ){
					found = true;
				}
			}
			
			if( !found ) {
				BattleshipView.saveMove( 'player_miss', explosions[j] );
				BattleshipView.addMiss( BattleshipView.opponentField, explosions[j], true );
			}
		}
	}
	else {
		for( var j = 0; j < explosions.length; j++ ) {
			BattleshipView.saveMove( 'player_miss', explosions[j] );
		}
	}
}

BattleshipView.calcCells = function(field, coords) {
	return {
		x : Math.round((Math.abs((field.x - field.width / 2) - coords.x) + BattleshipView.cellSize / 2)
						/ BattleshipView.cellSize) - 1,
		y : Math.round((Math.abs((field.y - field.width / 2) - coords.y) + BattleshipView.cellSize / 2)
						/ BattleshipView.cellSize) - 1
	};
}

BattleshipView.showYourTurnDialog = function() {
	BattleshipView.yourTurnDialog.dialog('open');
	
	var $div = $('#game_field_container');
    var left = $div.offset().left;
    var top= $div.offset().top;
    
    BattleshipView.yourTurnDialog.dialog( "option", "position", [left + 120, top + 160 ] );
}

BattleshipView.showMessageDialog = function(message) {
	BattleshipView.messageDialog.dialog('open');
}

BattleshipView.showLastmoveRes = function(result) {

	if( result == 0 ) {
		BattleshipView.saveMove('player_miss', BattleshipView.lastMove);
		BattleshipView.addMiss(BattleshipView.opponentField, BattleshipView.lastMove, true);
	}
	else {
		BattleshipView.saveMove('player_hit', BattleshipView.lastMove);
		BattleshipView.addHit(BattleshipView.opponentField, BattleshipView.lastMove, true);
	}
}

BattleshipView.showOpponentMove = function(px, py) {
	if (BattleshipView.checkHit(BattleshipView.playerShips, {
		x : px,
		y : py
	})) {
		BattleshipView.saveMove('opponent_hit', {
			x : px,
			y : py
		});
		BattleshipView.addHit(BattleshipView.playerField, {
			x : px,
			y : py
		}, true);
	} else {
		BattleshipView.saveMove('opponent_miss', {
			x : px,
			y : py
		});
		BattleshipView.addMiss(BattleshipView.playerField, {
			x : px,
			y : py
		}, true);
	}
}

BattleshipView.saveMove = function(key, move) {
	var saved = sessionStorage.getItem(key);
	if (saved == null)
		saved = "";
	saved += Math.ceil( move.x ) + Math.ceil( move.y ) * BattleshipView.fieldSizeCells + ',';
	sessionStorage.setItem(key, saved);
}

BattleshipView.checkMove = function( cell ) {
	if( ( player_misses = sessionStorage.getItem('player_miss') ) != null ) {
		if( BattleshipView.findCell( player_misses, cell ) )
			return false;
	}
	
	if( ( player_hits = sessionStorage.getItem('player_hits') ) != null ) {
		if( BattleshipView.findCell( player_hits, cell ) )
			return false;		
	}
	
	return true;
}

BattleshipView.findCell = function( collection, cell ) {
	for( var i = 0; i < collection.length; i++ ) {
		var coords = {
			x : Math.floor( collection[i] % BattleshipView.fieldSizeCells ),
			y : Math.floor( collection[i] / BattleshipView.fieldSizeCells )
		};
		
		if( ( cell.x == coords.x ) && ( cell.y == coords.y ) ){
			return true;
		}
	}
	
	return false;
}

// ==============================================================
// Events
//
BattleshipView.keypress = function(event) {
	if (event.which = 32) {
		if (BattleshipView.shipPlacementMode) {
			if (BattleshipView.placedShip) {
				if (!BattleshipView.placedShip.rotated) {
					BattleshipView.placedShip.rotate(90);
					BattleshipView.placedShip.rotated = true;
				} else {
					BattleshipView.placedShip.setOrigin()
					BattleshipView.placedShip.rotate(-90);
					BattleshipView.placedShip.rotated = false;
				}
			}
		}
	}
}

BattleshipView.mouseClick = function(event) {
	
	if( BattleshipClient.gameOverDialogShown ) {
		BattleshipClient.gameOverDialog.dialog('close');
		BattleshipClient.gameOver();
		return;
	}
	
	if (!BattleshipView.shipPlacementMode) {
		if (BattleshipClient.yourTurn) {
			if (this == BattleshipView.opponentField) {
				BattleshipView.lastMove = BattleshipView.calcCells(this, {
					x : event.x,
					y : event.y
				});
				
				// Check if there is already such move!
				if( BattleshipView.checkMove ) {
					sessionStorage.setItem('last_move', {
						x : event.x,
						y : event.y
					});
					BattleshipClient.sendPackage(BattleshipClient
							.createMyMovePacket(BattleshipView.lastMove));
					BattleshipClient.yourTurn = false;
					sessionStorage.setItem('your_turn', BattleshipClient.yourTurn);
					BattleshipView.yourTurnDialog.dialog('close');
				}
				else {
					alert( "Move invalid" );
				}
			}
		}
	}
}

BattleshipView.mouseDown = function(event) {
	if (BattleshipView.shipPlacementMode) {
		if (BattleshipView.placementCanvas.mouse.onCanvas()) {
			BattleshipView.placedShip = this;

			if (this.moveTo)
				this.moveTo(BattleshipView.placementCanvas.mouse.x,
						BattleshipView.placementCanvas.mouse.y);
		}
	}
}

BattleshipView.mouseUp = function(event) {
	// If ship rotated during movement mouseup doesn't fire!!!
	if (BattleshipView.shipPlacementMode) {
		if (BattleshipView.placementCanvas.mouse.onCanvas()) {
			if (BattleshipView.placedShip) {
				var cell_x = ( BattleshipView.placedShip.x - 15 ) / BattleshipView.cellSize;
				var cell_y = ( BattleshipView.placedShip.y - 10 ) / BattleshipView.cellSize;
				
				if (	!BattleshipView.indexValid(cell_x)
						|| !BattleshipView.indexValid(cell_y) ) {
					if( cell_x < 0 )
						cell_x = 1;
					
					if( cell_y < 0 )
						cell_y = 1;
					
					if( cell_y > 10 )
						cell_y = 10;
				}
				
				BattleshipView.placedShip.cell_x = Math.round(cell_x);
				BattleshipView.placedShip.cell_y = Math.round(cell_y);

				BattleshipView.placedShip.moveTo(
						(BattleshipView.placedShip.cell_x - 0.5) * BattleshipView.cellSize + 15,
						(BattleshipView.placedShip.cell_y - 0.5) * BattleshipView.cellSize + 10);

				BattleshipView.saveShipPos(BattleshipView.placedShip);
				BattleshipView.placedShip = null;
			}
		}
	}
}

//
// Events
// ==============================================================

BattleshipView.shipHit = function(ship, shoot) {
	for ( var i = 0; i < ship.length; i++) {
		if (!ship.rotated) {
			if (((ship.cell_x - 1) + i == shoot.x)
					&& ((ship.cell_y - 1) == shoot.y))
				return true;
		} else {
			if (((ship.cell_x - 1) == shoot.x)
					&& ((ship.cell_y - 1) + i == shoot.y))
				return true;
		}
	}

	return false;
}

BattleshipView.checkHit = function(ship_array, shoot) {
	for ( var i = 0; i < ship_array.length; i++) {
		if (BattleshipView.shipHit(ship_array[i], shoot))
			return true;
	}

	return false;
}

BattleshipView.getPlayerFieldString = function() {
	var ret = "";

	for ( var i = 0; i < BattleshipView.fieldSizeCells; i++) {
		for ( var j = 0; j < BattleshipView.fieldSizeCells; j++) {
			if (!BattleshipView.checkHit(BattleshipView.playerShips, {
				x : j,
				y : i
			})) {
				ret += '~';
			} else {
				ret += 'o';
			}
		}
	}

	return ret;
}

BattleshipView.shipsIntersect = function(ship1, ship2) {
	if (ship1.cell_x == -1)
		return false;

	if (ship2.cell_x == -1)
		return false;

	var ship1CoveredCells = new Array();

	if (!ship1.rotated) {
		for ( var i = -1; i <= ship1.length; i++) {
			ship1CoveredCells.push({
				cell_x : ship1.cell_x + i,
				cell_y : ship1.cell_y - 1
			});
			ship1CoveredCells.push({
				cell_x : ship1.cell_x + i,
				cell_y : ship1.cell_y
			});
			ship1CoveredCells.push({
				cell_x : ship1.cell_x + i,
				cell_y : ship1.cell_y + 1
			});
		}
	} else {
		for ( var i = -1; i <= ship1.length; i++) {
			ship1CoveredCells.push({
				cell_x : ship1.cell_x - 1,
				cell_y : ship1.cell_y + i
			});
			ship1CoveredCells.push({
				cell_x : ship1.cell_x,
				cell_y : ship1.cell_y + i
			});
			ship1CoveredCells.push({
				cell_x : ship1.cell_x + 1,
				cell_y : ship1.cell_y + i
			});
		}
	}

	if (!ship2.rotated) {
		for ( var i = 0; i < ship2.length; i++) {
			for ( var j = 0; j < ship1CoveredCells.length; j++) {
				if (((ship2.cell_x + i) == ship1CoveredCells[j].cell_x)
						&& (ship2.cell_y == ship1CoveredCells[j].cell_y)) {
					return true;
				}
			}
		}
	} else {
		for ( var i = 0; i < ship2.length; i++) {
			for ( var j = 0; j < ship1CoveredCells.length; j++) {
				if (((ship2.cell_y + i) == ship1CoveredCells[j].cell_y)
						&& (ship2.cell_x == ship1CoveredCells[j].cell_x)) {
					return true;
				}
			}
		}
	}

	return false;
}

BattleshipView.indexValid = function(index) {
	if ((index >= 0) && (index < BattleshipView.fieldSizeCells))
		return true;
	return false;
}

BattleshipView.getNextShipLength = function(cur_length, dir) {
	if (dir == "up") {
		if (cur_length == 1)
			return 2;
		else if (cur_length == 2)
			return 3;
		else if (cur_length == 3)
			return 4;
	} else if (dir == "down") {
		if (cur_length == 4)
			return 3;
		else if (cur_length == 3)
			return 2;
		else if (cur_length == 2)
			return 1;
	}
	return 0;
}

BattleshipView.getNumberOfShips = function(length) {
	if (length == 1)
		return 4;
	else if (length == 2)
		return 3;
	else if (length == 3)
		return 2;
	else if (length == 4)
		return 1;

	return 0;
}

BattleshipView.cellsChain = function( cell_1, cell_2 ) {
	if( ( Math.abs( cell_1 - cell_2 ) == 1 ) && ( Math.floor( cell_1/10 ) == Math.floor( cell_2/10 ) ) ) {
		return true;
	}
	
	if( ( Math.abs( cell_1 - cell_2 ) == 10 ) && ( Math.floor( cell_1%10 ) == Math.floor( cell_2%10 ) ) ){
		return true;
	}
	
	return false;
}

BattleshipView.shipCells = function( ship ) {
	var ret = new Array();
	
	var cell_x = ship.cell_x;
	var cell_y = ship.cell_y;
	var length = ship.length;
	
	for( var i = 0; i < length; i++ ) {
		if( ship.rotated ) {
			ret.push( { x : cell_x, y : cell_y + i } );
		}
		else {
			ret.push( { x : cell_x + i, y : cell_y } );
		}
	}
	
	return ret;
}

BattleshipView.shipBoundingCells = function( ship ) {
	var ret = new Array();
	
	var cell_x = ship.cell_x;
	var cell_y = ship.cell_y;
	var length = ship.length;
	
	if( ship.rotated ) {
		if( BattleshipView.indexValid( cell_y - 1 ) ) {
			ret.push( { x : cell_x, y : cell_y - 1 } );
			if( BattleshipView.indexValid( cell_x + 1 ) )
				ret.push( { x : cell_x + 1, y : cell_y - 1 } );
			if( BattleshipView.indexValid( cell_x - 1 ) )
				ret.push( { x : cell_x - 1, y : cell_y - 1 } );
		}
	}
	else {
		if( BattleshipView.indexValid( cell_x - 1 ) ) {
			ret.push( { x : cell_x - 1, y : cell_y } );
			if( BattleshipView.indexValid( cell_y + 1 ) )
				ret.push( { x : cell_x - 1, y : cell_y + 1 } );
			if( BattleshipView.indexValid( cell_y - 1 ) )
				ret.push( { x : cell_x - 1, y : cell_y - 1 } );
		}		
	}
	
	for( var i = 0; i < length; i++ ) {
		if( ship.rotated ) {
			if( BattleshipView.indexValid( cell_x - 1 ) )
				ret.push( { x : cell_x - 1, y : cell_y + i } );
			if( BattleshipView.indexValid( cell_x + 1 ) )
				ret.push( { x : cell_x + 1, y : cell_y + i } );
		}
		else {
			if( BattleshipView.indexValid( cell_y - 1 ) )
				ret.push( { x : cell_x + i, y : cell_y - 1 } );
			if( BattleshipView.indexValid( cell_y + 1 ) )
				ret.push( { x : cell_x + i, y : cell_y + 1 } );			
		}
	}
	
	if( ship.rotated ) {
		if( BattleshipView.indexValid( cell_y + length ) ) {
			ret.push( { x : cell_x, y : cell_y + length } );
			if( BattleshipView.indexValid( cell_x + 1 ) )
				ret.push( { x : cell_x + 1, y : cell_y + length } );
			if( BattleshipView.indexValid( cell_x - 1 ) )
				ret.push( { x : cell_x - 1, y : cell_y + length } );
		}
	}
	else {
		if( BattleshipView.indexValid( cell_x + length ) ) {
			ret.push( { x : cell_x + length, y : cell_y } );
			if( BattleshipView.indexValid( cell_y + 1 ) )
				ret.push( { x : cell_x + length, y : cell_y + 1 } );
			if( BattleshipView.indexValid( cell_y - 1 ) )
				ret.push( { x : cell_x + length, y : cell_y - 1 } );
		}		
	}
	
	return ret;
}

BattleshipView.getIndex = function( index_x, index_y ) {
	return index_y*10 + index_x;
}

BattleshipView.cellsTouch = function ( cell_1, cell_2 ) {
	var dx = Math.abs( getX( cell_1 ) - getX( cell_2 ) );
	var dy = Math.abs( getY( cell_1 ) - getY( cell_2 ) );
	
	return ( dx < 1 ) && ( dy < 1 );
}

BattleshipView.getX = function(index) {
	return Math.floor( Math.round( cell_1 )%10 );
}

BattleshipView.getY = function(index) {
	return Math.floor( Math.round( cell_1 )/10 );
}

BattleshipView.findLastInChain = function( chain, prev ) {
	for( var i = 0; i < chain.length; i++ ) {
		
		if( chain[i].length == 0 )
			continue;
		
		var x = Math.floor( chain[i]% BattleshipView.fieldSizeCells );
		var y = Math.floor( chain[i]/ BattleshipView.fieldSizeCells );
		
		cell = Math.round( x + y*10 );  
		
		if( BattleshipView.cellsChain( prev, cell ) ) {
			if( cell < prev )
				return BattleshipView.findLastInChain( chain, cell );
		}
	}
	
	return prev;
}

BattleshipView.findNextInChain = function( chain, prev ) {
	for( var i = 0; i < chain.length; i++ ) {
		
		var x = Math.floor( chain[i]% BattleshipView.fieldSizeCells );
		var y = Math.floor( chain[i]/ BattleshipView.fieldSizeCells );
		
		cell = Math.round( x + y*10 );  
		
		if( BattleshipView.cellsChain( prev, cell ) ) {
			return cell;
		}
	}
	
	return prev;
}
