//==============================================================
// BattleshipClient
//
function BattleshipClient() {
	BattleshipClient.inviteDialog = $('<div></div>')
			.html(
					"<p id='invite_label' ></p>\
            <p><input type='button' value='OK' onclick='BattleshipClient.inviteOK(BattleshipClient.inviteDialog.inviter)'>\
            </input>\
            <input type='button' value='Cancel' onclick='BattleshipClient.inviteCancel(BattleshipClient.inviteDialog.inviter)'>\
            </input></p>")
			.dialog({
				autoOpen : false,
				title : 'Invitation to game',
				width : 200,
				height : 200,
				modal : true
			});

	BattleshipClient.gameOverDialog = $('<div></div>')
			.html(
					"<p class='your_turn_mess'>GAME OVER!</p>\
            		<p id='game_over_message' ></p>")
			.dialog({
				autoOpen : false,
				title : 'Game over',
				width : 200,
				height : 150,
				draggable: false,
				resizable: false,
				modal : true,
				create: function(event, ui)  
				{
					$(this).parents(".ui-dialog").css("Background-color", "gray");
					$(this).parents(".ui-dialog").css("opacity", 0.7);
				}
			});

	BattleshipClient.gameKey = sessionStorage.getItem('game_key');
	BattleshipClient.playerKey = sessionStorage.getItem('player_key');
	BattleshipClient.myname = sessionStorage.getItem('my_name');
	BattleshipClient.yourTurn = sessionStorage.getItem('your_turn');
	
	BattleshipClient.setGameButtonsEnabled( sessionStorage.getItem('gameMode') );
	
	BattleshipClient.busySpinner = null;
	BattleshipClient.showBusyIndicator(true);
	
	$(".ui-dialog-titlebar").hide();
}

BattleshipClient.setGameButtonsEnabled = function(val) {
	$("#game_tell_button").prop("disabled", !val);
	$("#stop_game_button").prop("disabled", !val);
	
	if( !val ) {
		$("#game_tell_button").removeClass("tell_button");
		$("#game_tell_button").addClass("disabled_button");
		$("#stop_game_button").removeClass("close_button");
		$("#stop_game_button").addClass("disabled_button");
	}
	else {
		$("#game_tell_button").removeClass("disabled_button");
		$("#game_tell_button").addClass("tell_button");
		$("#stop_game_button").removeClass("disabled_button");
		$("#stop_game_button").addClass("close_button");
	}
}

BattleshipClient.showBusyIndicator = function(val) {
	
	if( BattleshipClient.busySpinner ==  null ) {
		var opts = {
				  lines: 11, // The number of lines to draw
				  length: 7, // The length of each line
				  width: 8, // The line thickness
				  radius: 24, // The radius of the inner circle
				  corners: 1, // Corner roundness (0..1)
				  rotate: 0, // The rotation offset
				  color: '#fff', // #rgb or #rrggbb
				  speed: 1, // Rounds per second
				  trail: 60, // Afterglow percentage
				  shadow: false, // Whether to render a shadow
				  hwaccel: false, // Whether to use hardware acceleration
				  className: 'spinner', // The CSS class to assign to the spinner
				  zIndex: 2e9, // The z-index (defaults to 2000000000)
				  top: 'auto', // Top position relative to parent in px
				  left: 'auto' // Left position relative to parent in px
				};
		
		BattleshipClient.busySpinner = new Spinner(opts);
	}
	
	var target = document.getElementById('game_field_container');

	if( val )
		BattleshipClient.busySpinner.spin(target);
	else
		BattleshipClient.busySpinner.stop();
	
	$("#send_button").prop("disabled", val);
}

BattleshipClient.gameOver = function() {
	for ( var i = 0; i < BattleshipView.playerShips.length; i++) {
		BattleshipView.canvas.removeChild(BattleshipView.playerShips[i]);
	}
	
	for ( var i = 0; i < BattleshipView.opponentShips.length; i++) {
		BattleshipView.canvas.removeChild(BattleshipView.opponentShips[i]);
	}

	// remove all the explosions
	for ( var i = 0; i < BattleshipView.explosions.length; i++) {
		BattleshipView.canvas.removeChild(BattleshipView.explosions[i]);
	}

	sessionStorage.setItem('gameMode', false);
	sessionStorage.setItem('placementMode', false);
	sessionStorage.setItem('your_turn', false);
	BattleshipClient.gameOverDialog.dialog('close');

	sessionStorage.setItem('player_miss', "");
	sessionStorage.setItem('player_hit', "");
	sessionStorage.setItem('opponent_miss', "");
	sessionStorage.setItem('opponent_hit', "");
	
	BattleshipClient.setGameButtonsEnabled(false);
	BattleshipClient.yourTurn = false;
	
	BattleshipView.opponentShips = new Array();
}

BattleshipClient.leave = function() {
	BattleshipClient.sendSynchronious(BattleshipClient.createLeavePackage());
}

BattleshipClient.playerList = new Array();

BattleshipClient.fillTable = function(array) {
	$("#table_body").find('tr').remove();
	for ( var i = 0; i < array.length; i++) {
		if( array[i]['name'] == BattleshipClient.myname ) {
			$("#player_table > tbody:last").append(
					"<tr><td class='player_name_td'>" + array[i]['name'] + "</td>"
							+ "<td></td>" +
							"<td></td></tr>");			
		}
		else if( array[i]['ready'] == "n") {
			$("#player_table > tbody:last").append(
					"<tr><td class='player_name_td'>" + array[i]['name'] + "</td>"
							+ "<td><input type='button' value='' class='disabled_button'"
							+ "id=invite" + i + " disabled='disabled'"
							+ "></input></td>" +
							"<td><input type='button' value='' class='tell_button' onclick='BattleshipClient.onTellClick('" + i
							+ "')'></input></td></tr>");
		} else {
			$("#player_table > tbody:last").append(
					"<tr><td class='player_name_td'>" + array[i]['name'] + "</td>"
							+ "<td><input type='button' value='' class='game_button'"
							+ "id=invite" + i
							+ " onclick='BattleshipClient.onInviteClick(" + i
							+ ")'" + "></input></td>" +
							"<td><input type='button' value='' class='tell_button' onclick='BattleshipClient.onTellClick(" + i 
							+ ")'></input></td></tr>");
		}
	}
}

BattleshipClient.onInviteClick = function(index) {
	BattleshipClient.sendPackage(BattleshipClient
			.createInvitePackage(BattleshipClient.playerList[index]['name']));
}

BattleshipClient.onTellClick = function( index ) {
	PlayshipsChat.privateMessTo( BattleshipClient.playerList[index]['name'] );
}

BattleshipClient.privateGameMess = function() {
	PlayshipsChat.privateMessTo( BattleshipClient.opponentName );
}

BattleshipClient.stopGame = function() {
	BattleshipClient.sendPackage( BattleshipClient.createStopGamePackage() );
	BattleshipClient.gameOver();
}

var server_adress = //"http://localhost:8888/game";
	"http://playships.appspot.com/game";

BattleshipClient.sendPackage = function(package) {
	$.post(server_adress, package,
	BattleshipClient.responseReceived);
}

BattleshipClient.sendSynchronious = function(package) {
	$.ajax({
		  type: 'POST',
		  url: server_adress,
		  data: package,
		  success: BattleshipClient.responseReceived,
		  cache: false,
		  async:false
	});
}

BattleshipClient.clientInvitePacket = 2;
BattleshipClient.clientInviteAnswerPacket = 7;
BattleshipClient.clientAmReadyPacket = 5;
BattleshipClient.clientMyMovePacket = 3;
BattleshipClient.clientLeavePacket = 8;
BattleshipClient.clientEnterPackage = 12;
BattleshipClient.clientPlayerlistPackage = 11;
BattleshipClient.clientStopGamePacket = 13;
BattleshipClient.clientRequestTokenPaket = 14;

BattleshipClient.createInvitePackage = function(pname) {
	return {
		type : BattleshipClient.clientInvitePacket,
		name : pname,
		playerKey : BattleshipClient.playerKey
	};
}

BattleshipClient.createInviteAnswerPacket = function(pdesision, pname) {
	return {
		type : BattleshipClient.clientInviteAnswerPacket,
		desision : pdesision,
		name : pname,
		playerKey : BattleshipClient.playerKey
	};
}

BattleshipClient.createShipsPlacedPacket = function(ships_string) {
	return {
		type : BattleshipClient.clientAmReadyPacket,
		gameKey : BattleshipClient.gameKey,
		playerKey : BattleshipClient.playerKey,
		field : ships_string
	};
}

BattleshipClient.createMyMovePacket = function(coords) {
	return {
		type : BattleshipClient.clientMyMovePacket,
		playerKey : BattleshipClient.playerKey,
		gameKey : BattleshipClient.gameKey,
		x : coords.x,
		y : coords.y
	};
}

BattleshipClient.createLeavePackage = function() {
	return {
		type : BattleshipClient.clientLeavePacket,
		playerKey : BattleshipClient.playerKey
	};
}

BattleshipClient.createEnterPackage = function() {
	return {
		type : BattleshipClient.clientEnterPackage
	};
}

BattleshipClient.createPlayerListPackage = function() {
	return {
		type : BattleshipClient.serverPlayerlistPacket,
		playerKey : BattleshipClient.playerKey
	};
}

BattleshipClient.createStopGamePackage = function() {
	return {
		type : BattleshipClient.clientStopGamePacket,
		playerKey : BattleshipClient.playerKey,
		gameKey : BattleshipClient.gameKey
	}
}

BattleshipClient.clientRequestTokenPackage = function() {
	return {
		type : BattleshipClient.clientRequestTokenPaket,
		playerKey : BattleshipClient.playerKey
	}
}

BattleshipClient.serverPlayerEntered = 1;
BattleshipClient.serverPlayerLeaved = 8;
BattleshipClient.serverGameStartPacket = 4;
BattleshipClient.serverOpponentMovePacket = 10;
BattleshipClient.serverMoveResultPacket = 6;
BattleshipClient.serverTokenPacket = 12;
BattleshipClient.serverYouInvitedPacket = 2;
BattleshipClient.serverYourTurnPacket = 5;
BattleshipClient.serverGameOverPacket = 7;
BattleshipClient.serverMessagePacket = 9;
BattleshipClient.serverPlayerlistPacket = 11;
BattleshipClient.serverPrivateMessagePacket = 13;
BattleshipClient.serverFieldBadPacket = 15;
BattleshipClient.serverGameCancelledPacket = 16;
BattleshipClient.serverPlayerStatusChanged = 17;
BattleshipClient.serverInfoPacket = 18;
BattleshipClient.serverNewToken = 19;
BattleshipClient.serverOvercrowdedPacket = 20;
BattleshipClient.serverBannedPacket = 21;

BattleshipClient.yourTurn = false;
BattleshipClient.myname = "";
BattleshipClient.opponentName = "";
BattleshipClient.gameOverDialogShown = false;

BattleshipClient.playerKey = "";// User key
BattleshipClient.gameKey = ""; // Game key

BattleshipClient.responseReceived = function(data) {
	if (data.length == 0)
		return;

	var packet = eval("(" + data + ")");

	BattleshipClient.handleResponse(packet);
}

BattleshipClient.handleResponse = function(packet) {
	if (packet.type == BattleshipClient.serverGameStartPacket) {
		/*
		 * Game started, show the player dialog to position his ships
		 */
		var player1 = packet.player1;
		var player2 = packet.player2;
		BattleshipClient.gameKey = packet.gameKey;
		BattleshipClient.opponentName = (BattleshipClient.myname == player1) ? player2 : player1;

		sessionStorage.setItem('opponent_name', BattleshipClient.opponentName);
		sessionStorage.setItem('game_key', BattleshipClient.gameKey);

		BattleshipView.startPlacementMode(BattleshipClient.opponentName);

		BattleshipClient.setGameButtonsEnabled(true);
	} else if (packet.type == BattleshipClient.serverGameOverPacket) {
		BattleshipClient.showGameOverDialog();
	}
	else if (packet.type == BattleshipClient.serverMoveResultPacket) {
		BattleshipView.showLastmoveRes(packet.result);
		
		if( packet.result > 1 ) {
			BattleshipView.addKilledOpponentShip( packet.result - 1 );
		}
		
		if( packet.yourTurn ) {
			if (!BattleshipClient.yourTurn) {
				BattleshipView.showYourTurnDialog();
				BattleshipClient.yourTurn = true;
				sessionStorage.setItem('your_turn', BattleshipClient.yourTurn);
			}
		}
	} else if (packet.type == BattleshipClient.serverTokenPacket) {
		BattleshipClient.openChannel(packet.token);

		BattleshipClient.myname = packet.name;
		BattleshipClient.playerKey = packet.playerKey;
		sessionStorage.setItem('player_key', BattleshipClient.playerKey);
		sessionStorage.setItem('my_name', BattleshipClient.myname);
		
		PlayshipsChat.addTail( packet.tail );

		BattleshipClient.loadPlayerList(packet);
	} else if (packet.type == BattleshipClient.serverYouInvitedPacket) {
		var inviter = packet.inviter;

		if (inviter.length > 0) {
			$('#invite_label').text(inviter + " invites you to play");
			BattleshipClient.inviteDialog.inviter = inviter;
			BattleshipClient.inviteDialog.dialog('open');
		}
	} else if (packet.type == BattleshipClient.serverYourTurnPacket) {
		if (!BattleshipClient.yourTurn) {
			BattleshipView.showYourTurnDialog();
			BattleshipClient.yourTurn = true;
			sessionStorage.setItem('your_turn', BattleshipClient.yourTurn);
		}
	} else if (packet.type == BattleshipClient.serverPlayerEntered) {
		for ( var i = 0; i < BattleshipClient.playerList.length; i++) {
			if (BattleshipClient.playerList[i].name == packet.name) {
				return;
			}
		}

		BattleshipClient.playerList.push({
			name : packet.name,
			score : 0,
			ready : 'r'
		});
		BattleshipClient.fillTable(BattleshipClient.playerList);
	} else if (packet.type == BattleshipClient.serverPlayerLeaved) {
		for ( var i = 0; i < BattleshipClient.playerList.length; i++) {
			if (BattleshipClient.playerList[i].name == packet.name) {
				BattleshipClient.playerList[i] = null;
				BattleshipClient.playerList.splice(i, 1);
				break;
			}
		}

		BattleshipClient.fillTable(BattleshipClient.playerList);
	} else if (packet.type == BattleshipClient.serverOpponentMovePacket) {
		if (packet.x != null) {
			if (packet.y != null) {
				BattleshipView.showOpponentMove(packet.x, packet.y);
			}
		}
		
		if( packet.yourTurn ) {
			if (!BattleshipClient.yourTurn) {
				BattleshipView.showYourTurnDialog();
				BattleshipClient.yourTurn = true;
				sessionStorage.setItem('your_turn', BattleshipClient.yourTurn);
			}
		}
	} else if (packet.type == BattleshipClient.serverMessagePacket) {
		PlayshipsChat.chatUpdateReceived(packet, false);
	} else if (packet.type == BattleshipClient.serverPlayerlistPacket) {
		BattleshipClient.loadPlayerList(packet);
	}
	else if( packet.type == BattleshipClient.serverPrivateMessagePacket ) {
		PlayshipsChat.chatUpdateReceived(packet, true);
	}
	else if( packet.type == BattleshipClient.serverFieldBadPacket ) {
		sessionStorage.setItem('placementMode', false);
		alert( "Your ships position invalid!!" );
		BattleshipView.startPlacementMode(BattleshipClient.opponentName);
	}
	else if( packet.type == BattleshipClient.serverGameCancelledPacket ) {
		alert( "Game cancelled by" + packet.name );
		BattleshipClient.gameOver();
	}
	else if( packet.type == BattleshipClient.serverPlayerStatusChanged ) {
		var status = packet.ready;
		var name = packet.name;
		
		BattleshipClient.setPlayersStatus( name, status );
	}
	else if( packet.type == BattleshipClient.serverNewToken ) {
		BattleshipClient.openChannel(packet.token);
	}
	else if( packet.type == BattleshipClient.serverInfoPacket ) {
		alert( packet.info );
	}
	else if( packet.type == BattleshipClient.serverOvercrowdedPacket ) {
		$('.brave-fitter').replaceWith("<div class='no_enter'><h1>SERVER OVERCROWDED, VISIT LATER!!!</h1></div>");
	}
	else if( packet.type == BattleshipClient.serverBannedPacket ) {
		$('.brave-fitter').replaceWith("<div class='no_enter'><h1>SORRY, YOU'VE BEEN TEMPORARILY BANNED!!!</h1></div>");
	}
	else {
		alert('unknown packet');
	}
}

BattleshipClient.showGameOverDialog = function() {
	BattleshipClient.gameOverDialog.dialog('open');

	var $div = $('#game_field_container');
	var left = $div.offset().left;
	var top= $div.offset().top;

	BattleshipClient.gameOverDialog.dialog( "option", "position", [left + 250, top + 130 ] );
	
	BattleshipClient.gameOverDialogShown = true;
	
	setTimeout(function(){
		BattleshipClient.gameOverDialog.dialog('close');   
		BattleshipClient.gameOverDialogShown = false;
		BattleshipClient.gameOver();
	}, 3000);
}

BattleshipClient.loadPlayerList = function(packet) {
	if (packet.playerList.length > 0) {
		BattleshipClient.playerList = [];
		var player_strings = packet.playerList.split(',');

		for ( var i = 0; i < player_strings.length; i++) {
			if (player_strings[i].length == 0)
				break;

			var player_data = player_strings[i].split(' ');
			BattleshipClient.playerList.push({
				name : player_data[0],
				score : 0,
				ready : player_data[1]
			});
		}

		BattleshipClient.fillTable(BattleshipClient.playerList);
	}
}

BattleshipClient.setPlayersStatus = function( name, status ) {
	for( var k = 0; k < BattleshipClient.playerList.length; k++ ) {
		if( name == BattleshipClient.playerList[k].name ) {
			if( status )
				BattleshipClient.playerList[k].ready = "r";
			else
				BattleshipClient.playerList[k].ready = "n"
		}
	}
	
	BattleshipClient.fillTable(BattleshipClient.playerList);
}

BattleshipClient.refreshPlayerList = function() {
	BattleshipClient.sendPackage(BattleshipClient.createPlayerListPackage());
}

BattleshipClient.openChannel = function(token) {
	var channel = new goog.appengine.Channel(token);

	var handler = {
		'onopen' : BattleshipClient.channelOpened,
		'onmessage' : BattleshipClient.channelMessage,
		'onerror' : function() {
			// request new token
			BattleshipClient.sendPackage( BattleshipClient.clientRequestTokenPackage() );
		},
		'onclose' : function() {
		}
	};
	var socket = channel.open(handler);
}

BattleshipClient.channelOpened = function() {
	BattleshipClient.showBusyIndicator(false);
}

BattleshipClient.channelMessage = function(mess) {
	var message = eval("(" + mess.data + ")");
	BattleshipClient.handleResponse(message);
}

BattleshipClient.inviteOK = function(inviter) {
	BattleshipClient.sendPackage(BattleshipClient.createInviteAnswerPacket(
			true, inviter));
	BattleshipClient.inviteDialog.dialog('close');
}

BattleshipClient.inviteCancel = function() {
	BattleshipClient.sendPackage(BattleshipClient.createInviteAnswerPacket(
			false, inviter));
	BattleshipClient.inviteDialog.dialog('close');
}
