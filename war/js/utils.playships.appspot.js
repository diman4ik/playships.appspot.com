
function Utils() {
	Utils.setListPosition(0);
}

Utils.list_position = 0;
Utils.page_size = 0;
Utils.total_size = 0;
Utils.direction = "forward";

var server_adress = //"http://localhost:8888/admin/bahn";
	"http://playships.appspot.com/admin/bahn";

Utils.sendPackage = function(package) {
	$.post( server_adress, package, Utils.responseReceived );
}

Utils.createBahnPackage = function(pname) {
	return {
		type: "bahn",
		name : pname
	};
}

Utils.createUnBahnPackage = function(pname) {
	return {
		type: "unbahn",
		name : pname
	};
}

Utils.createListPackage = function( pfrom, pcount, pdirection ) {
	return {
		type: "list",
		from: pfrom,
		count: pcount,
		direction: pdirection
	};
}

Utils.createMaxUsersPackage = function(num) {
	return {
		type: "max_users",
		count: num
	};
}

Utils.loadAllUsers = function(param) {
	Utils.sendPackage( Utils.createListPackage() );
}

Utils.responseReceived = function(data) {
	if( data.length > 0 ) {
		var plist = eval("(" + data + ")");
		
		$("#bahn_list tbody").empty();
		
		for( var i = 0; i < plist.length; i++ ) {
			var next = plist[i];
			
			$("#bahn_list > tbody:last").append(
				"<tr><td>" + next.name + "</td>"	+ "<td>" + next.nstarted + "</td>" + 
				"<td>" + next.nfinished + "</td><td>" + 
				( ( next.banned )? 	"<input type='button' value='Розбанить' onclick='Utils.bahn(" + i + ");'/>" :
									"<input type='button' value='Зобанить' onclick='Utils.bahn(" + i + ");'/>" ) + "</td></tr>" );
		}
	}
}

Utils.bahn = function( index ) {
	// table bahn_list select
	var row = $('#bahn_list tr').eq( index + 1 )[0];
	var name = row.cells[0].innerHTML;
	
	var input = $( row.cells[3] ).find('input');
	
	if( input.val() == "Зобанить" ) {
		Utils.sendPackage( Utils.createBahnPackage(name) );
		input.val( "Розбанить" );
	}
	else if( input.val() == "Розбанить" ) {
		Utils.sendPackage( Utils.createUnBahnPackage(name) );
		input.val( "Зобанить" );
	}
}

Utils.setUserNum = function() {	
	var count = $( "#user_count" ).val();
	
	if( count > 0 ) {
		Utils.sendPackage( Utils.createMaxUsersPackage(count) );
	}
}

Utils.next = function() {
	Utils.sendPackage( Utils.createListPackage( Utils.list_position + Utils.page_size, Utils.page_size ) );
	Utils.direction = "forward";
	
	Utils.setListPosition( Utils.list_position + Utils.page_size );
}

Utils.prev = function() {
	Utils.sendPackage( Utils.createListPackage( Utils.list_position - Utils.page_size, Utils.page_size ) );
	Utils.direction = "backward";
	
	Utils.setListPosition( Utils.list_position - Utils.page_size );
}

Utils.setListPosition = function( position ) {
	
	if( ( position < 0 ) || ( position > Utils.total_size ) ) {
		return;
	}
		
	Utils.list_position = position;
	
	if( position <= 0 ) {
		$(".prev_players").attr("disabled", "disabled");
		$(".prev_players").hide();
	}
	else if( position > 0 ){
		$(".prev_players").removeAttr("disabled");
		$(".prev_players").show();
	}
	
	if( position > 0 ) {
		if( position >= Utils.total_size - Utils.page_size ) {
			$(".next_players").attr("disabled", "disabled");
			$(".next_players").hide();
		}
		else if( position <= Utils.total_size - Utils.page_size ){
			$(".next_players").removeAttr("disabled");
			$(".next_players").show();
		}
	}
}
