﻿
//==============================================================
// Chat
//

function PlayshipsChat() {
	chatWidget();
}

PlayshipsChat.sendMessage = function( pack ) {
	$.post( "http://localhost:8888/chat", pack );
	//$.post( "http://playships.appspot.com/chat", pack );
}								

PlayshipsChat.createChatMessage = function( data ) {
	
	if( data[0] == "all"){
		return {
			type: "common",
			text: data[1] };
	}
	
	return {
		type: "private",
		adress: data[0],
		message : data[1]
	};
}

PlayshipsChat.createTailMessage = function() {
	return {
		type: "get_tail"
	}
}

PlayshipsChat.chatUpdateReceived = function(mess, priv) {
	if( priv ) {
		var text = "/from " + '"' + mess.name + '"' + ":" + mess.message;
		chatWidget.addMessage( text, "red" );
	}
	else {
		var text = mess.name + ":" + mess.message;
		chatWidget.addMessage( text, null );
	}
}

PlayshipsChat.addTail = function( tail ) {
	var messages = tail.split(";.;");
	
	for( var i = 0; i < messages.length; i++ ) {
		if( messages[i].length > 0 )
			chatWidget.addMessage( messages[i], null );
	}
}

PlayshipsChat.privateMessTo = function(name) {
	chatWidget.commonChat.writeTo(name);
}
							
function chatWidget() {
	chatWidget.commonChat = new chat( $("#chat_area"), $("#send_game") );
}

chatWidget.addLine = function( line ) {
	chatWidget.commonChat.addLine(line);
}

chatWidget.addMessage = function( message, color ) {
	chatWidget.commonChat.addMessage(message, color);
}

chatWidget.sendPress = function() {
	var data = chatWidget.commonChat.getMessage();
	PlayshipsChat.sendMessage( PlayshipsChat.createChatMessage(data) );
	$("#send_game").val("");
}

function chat(container, editor) {
	this.container = container;
	this.editor = editor;
	this.loadNewLines = chat.loadNewLines;
	this.addLine = chat.addLine;
	this.getMessage = chat.getMessage;
	this.messages = new Array();
	this.addMessage = chat.addMessage;
	this.writeTo = chat.writeTo;
}

chat.loadNewLines = function(lines) {
	(this.container).empty();
	for( var i = 0; i < lines.length; i++ ) {
		(this.container).append( lines[i] + "<br>" );
	}
}

chat.addLine = function(line, color) {
	if( color != null )
		(this.container).append( "<span style='color:" + color + "'>" + line + "</span><br>" );
	else
		(this.container).append( line + "<br>" );
	(this.container[0]).scrollTop = (this.container[0]).scrollHeight;
}

chat.addMessage = function(message, color) {
	if( (this.messages).length > 100 ) {
		(this.messages).shift();
		(this.container).children(':first-child').remove();
	}

	(this.messages).push(message);
	
	this.addLine( message, color );
}

chat.writeTo = function(name) {
	$(this.editor).val("/to " + '"' + name + '"' + ":");
}

chat.getMessage = function() {
	var all = $(this.editor).val();
	
	if( all.indexOf("/to ") == 0 ) {
		var name_begin_ind = 5;
		var name_end_ind = all.indexOf( '"', name_begin_ind );
		var mess_begin_ind = all.indexOf(":");
		
		var adresat = all.substr( name_begin_ind, name_end_ind - name_begin_ind );
		var message = all.substr( name_end_ind + 2 );
		this.addLine( all, "red" );
		return [ adresat, message ];
	}
	
	return [ "all", all ];
}

//
// Chat
//==============================================================
